-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2019 at 10:19 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `accounting`
--

-- --------------------------------------------------------

--
-- Table structure for table `cost`
--

CREATE TABLE `cost` (
  `id` int(255) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` int(255) NOT NULL DEFAULT '0',
  `comment` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cost`
--

INSERT INTO `cost` (`id`, `date`, `amount`, `comment`) VALUES
(5, '2019-03-16 13:15:33', 1200, 'chair');

-- --------------------------------------------------------

--
-- Table structure for table `loan`
--

CREATE TABLE `loan` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `amount` int(255) NOT NULL,
  `return_amount` int(255) NOT NULL DEFAULT '0',
  `sanction_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `return_date` varchar(255) DEFAULT NULL,
  `profit` int(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loan`
--

INSERT INTO `loan` (`id`, `user_id`, `amount`, `return_amount`, `sanction_date`, `return_date`, `profit`) VALUES
(6, 2, 3000, 1100, '2019-03-16 12:55:16', NULL, 100),
(7, 2, 1000, 1200, '2019-03-16 13:25:04', NULL, 200),
(8, 2, 5000, 0, '2019-03-21 11:26:35', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `member_share`
--

CREATE TABLE `member_share` (
  `id` int(255) NOT NULL,
  `member_id` int(255) NOT NULL,
  `share_number` int(255) DEFAULT NULL,
  `share_amount` int(255) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member_share`
--

INSERT INTO `member_share` (`id`, `member_id`, `share_number`, `share_amount`, `date`) VALUES
(7, 2, 10, 10000, '2019-03-16 12:53:19');

-- --------------------------------------------------------

--
-- Table structure for table `savings`
--

CREATE TABLE `savings` (
  `id` int(255) NOT NULL,
  `member_id` int(255) NOT NULL,
  `saving_amount` int(255) NOT NULL,
  `day` int(255) DEFAULT NULL,
  `month` int(255) NOT NULL,
  `year` int(255) NOT NULL,
  `ref_number` varchar(255) DEFAULT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `savings`
--

INSERT INTO `savings` (`id`, `member_id`, `saving_amount`, `day`, `month`, `year`, `ref_number`, `time`) VALUES
(9, 2, 1000, NULL, 1, 2019, 'bkas123', '2019-03-16 12:54:54'),
(11, 2, 0, NULL, 2, 2019, NULL, '2019-03-18 12:38:21');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `data_id` varchar(255) NOT NULL,
  `data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `data_id`, `data`) VALUES
(1, 'phone', '+1737963893'),
(2, 'logo', 'logo'),
(3, 'site_title', 'Co-operative Society Management'),
(4, 'header_bottom_bg', ''),
(5, 'email', 'info@hotel.com'),
(6, 'icon', ''),
(7, 'facebook_like_page', '           	 			<div id=\"fb-root\"></div> <script>(function(d, s, id) {   var js, fjs = d.getElementsByTagName(s)[0];   if (d.getElementById(id)) return;   js = d.createElement(s); js.id = id;   js.src = \"//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5\";   fjs.parentNode.insertBefore(js, fjs); }(document, \'script\', \'facebook-jssdk\'));</script> <div class=\"fb-page\" data-href=\"https://www.facebook.com/webnestuk/\" data-small-header=\"false\" data-adapt-container-width=\"true\" data-hide-cover=\"false\" data-show-facepile=\"true\"><div class=\"fb-xfbml-parse-ignore\"><blockquote cite=\"https://www.facebook.com/webnestuk/\"><a href=\"https://www.facebook.com/webnestuk/\">Web Nest</a></blockquote></div></div>'),
(8, 'g_plus', 'www.gogle.com'),
(9, 'youtube', 'www.gogle.com'),
(10, 'twitter', 'www.twitter.com'),
(11, 'facebook', 'www.facebook.com'),
(12, 'pinterest_client_id', '4994146190405693516'),
(13, 'pinterest_client_secret', '88cb11e6f35a06c07fd67bf88b26053fc91576abc9141e2195156899f40b2365'),
(14, 'instagram_user_name', 'ashraful_eng'),
(15, 'instagram_password', 'Hello123#D'),
(16, 'instagram_client_id', '642a5791f78847c5864e69a79ade9cd2'),
(17, 'instagram_client_secret', '58b1a5b42bac43978b54ae2933c8a6a6'),
(18, 'facebook_app_id', '253329952047295'),
(19, 'facebook_app_secret', 'b75d9a3200df685718959ae7c9cb7d9e'),
(20, 'member_admission_fee', '1000'),
(21, 'total_share', '1000'),
(22, 'share_price', '1000'),
(23, 'monthly_savings', '100');

-- --------------------------------------------------------

--
-- Table structure for table `society`
--

CREATE TABLE `society` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `admin_id` int(255) DEFAULT NULL,
  `total_fund` int(255) DEFAULT NULL,
  `share_rate` int(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `society`
--

INSERT INTO `society` (`id`, `name`, `description`, `admin_id`, `total_fund`, `share_rate`, `type`) VALUES
(1, 'Society Name', 'Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. ', 10, NULL, 0, 'public'),
(2, 'Society Name', 'Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. Society Description. ', 10, NULL, 0, 'public'),
(3, 'BackstreetBoyes', 'This is a public Society, where anyone can get a member......................\r\n........................', 12, 5000000, 500, 'public');

-- --------------------------------------------------------

--
-- Table structure for table `society_members`
--

CREATE TABLE `society_members` (
  `id` int(255) NOT NULL,
  `society_id` int(255) NOT NULL,
  `member_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(100) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'customer',
  `image` varchar(255) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `country` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `p_c_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nid_number` varchar(255) DEFAULT NULL,
  `passport_number` varchar(255) DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `referal_id` int(255) DEFAULT NULL,
  `team` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'inactive',
  `joined` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `admission_fee` int(255) DEFAULT NULL,
  `society_id` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `user_name`, `password`, `type`, `image`, `address1`, `address2`, `phone`, `email`, `country`, `city`, `state`, `birthday`, `p_c_date`, `nid_number`, `passport_number`, `product_code`, `referal_id`, `team`, `status`, `joined`, `admission_fee`, `society_id`) VALUES
(1, 'Rumon', 'Hussain', 'admin', '202cb962ac59075b964b07152d234b70', 'admin', '15531015963.jpg', 'address', 'address', '', 'sharmin@gmail.com', 'bangladesh', 'Sylhet', 'sylhet', '', '2017-10-24 11:41:03', '', '', NULL, NULL, NULL, 'invective', '2017-10-24 11:41:03', NULL, 1),
(2, 'Test User', 'Test User', 'test_user', '202cb962ac59075b964b07152d234b70', 'member', 'demo-avater.png', 'Shahporan, Sadar, Sylhet', '326, 2rd floor, Rang-mahal Tower Bondor Bazar, Sylhet, Bangladesh', '01759318684', 'test@gmail.com', 'bangladesh', 'Moulvibazar', 'Juri', '01/23/2019', '2019-01-04 14:24:33', '', '', NULL, NULL, NULL, 'inactive', '2019-01-04 14:24:33', 1000, 1),
(3, 'Rumon', 'Hussain', 'rumonz', '202cb962ac59075b964b07152d234b70', 'member', '15480516392.jpg', 'Sylhet', '326, 2rd floor, Rang-mahal Tower Bondor Bazar, Sylhet, Bangladesh', '01759318684', 'rumn@gmail.com', 'bangladesh', 'sylhet', 'Sylhet', '01/02/2019', '2019-01-21 06:19:11', '', '', NULL, NULL, NULL, 'inactive', '2019-01-21 06:19:11', 1000, 1),
(4, 'Sumaya', 'Islam', 'sumaya1', '25d55ad283aa400af464c76d713c07ad', 'member', '1553101719images.jpg', 'karughor, mitalimantion, zinfabazar, sylhet', '326, 2rd floor, Rang-mahal Tower Bondor Bazar, Sylhet, Bangladesh', '01759318684', 'rufjffhj@gmail.com', 'bangladesh', 'Sylhet', 'Sylhet', '02/06/2008', '2019-03-20 07:07:24', '01752345866', '', NULL, NULL, NULL, 'inactive', '2019-03-20 07:07:24', 1000, 1),
(5, 'Majedur ', 'Rahman', 'majedur', '25d55ad283aa400af464c76d713c07ad', 'member', '1553101686download (1).jpg', 'karughor, mitalimantion, zinfabazar, sylhet', '326, 2rd floor, Rang-mahal Tower Bondor Bazar, Sylhet, Bangladesh', '01759318684', 'majedur@gmail.com', 'bangladesh', 'Dhaka', 'Dhaka', '03/07/1998', '2019-03-20 17:01:11', '', '', NULL, NULL, NULL, 'inactive', '2019-03-20 17:01:11', 1000, 1),
(6, 'Numan ', 'Rashid', 'numan', 'fcea920f7412b5da7be0cf42b8c93759', 'member', '1553101659download.jpg', 'Hotel Ishaqi Amos in sreemongal', '326, 2rd floor, Rang-mahal Tower Bondor Bazar, Sylhet, Bangladesh', '01759318684', 'numan@gmail.com', 'bangladesh', 'Sylhet', 'Dhaka', '02/26/2019', '2019-03-20 17:07:33', '', '', NULL, NULL, NULL, 'inactive', '2019-03-20 17:07:33', 1000, 1),
(7, 'Rahul', 'Ahmed', 'rahul', '25d55ad283aa400af464c76d713c07ad', 'member', 'demo-avater.png', 'sylhet', NULL, NULL, 'rahul@gmail.com', NULL, 'Sylhet', 'sylhet', '03/06/2020', '2019-03-21 05:12:15', NULL, NULL, NULL, NULL, NULL, 'inactive', '2019-03-21 05:12:15', 1000, 1),
(8, 'Ahmed', 'Emon', 'emon', 'c4ca4238a0b923820dcc509a6f75849b', 'member', 'demo-avater.png', 'sylhet', NULL, NULL, 'emon@gmail.com', NULL, 'Sylhet', 'Dhaka', '03/05/2019', '2019-03-21 05:15:27', NULL, NULL, NULL, NULL, NULL, 'inactive', '2019-03-21 05:15:27', 1000, 1),
(9, 'Abdul', 'Kalam', 'kalam', '25d55ad283aa400af464c76d713c07ad', 'member', 'demo-avater.png', 'sylhet', NULL, NULL, 'kalam@gmail.com', NULL, 'sylhet', 'sylhet', '03/14/2019', '2019-03-21 05:19:13', NULL, NULL, NULL, NULL, NULL, 'inactive', '2019-03-21 05:19:13', 2000, 1),
(10, 'Rumon', 'Hussain', 'rumon', '9d981eca92d1744276e9afc2acc68335', 'member', '1556001510HUMAYUN KABIR 2.jpg', 'jidabazar', NULL, '01751381331', 'rumon@gmail.com', 'Bangladesh', 'Sylhet', 'Sylhet Sadar', '02/02/1999', '2019-04-23 06:38:30', '17737267467464', NULL, NULL, NULL, NULL, 'inactive', '2019-04-23 06:38:30', 1000, 1),
(11, 'Hello', 'Ashraful', 'reza', 'd1f81f4539d95ffb125989440ee4a285', 'member', '1556216762ashraful-islam.jpg', 'Test Address Hello 123', NULL, '01751381331', 'info@hello123.com', 'Bangladesh', 'Sylhet', 'Sylhet Sadar', '04/18/2001', '2019-04-25 18:26:02', '34343434', NULL, NULL, NULL, NULL, 'inactive', '2019-04-25 18:26:02', 1000, NULL),
(12, 'rumon', 'bokth', 'bokth', '3e6a00a36dc60a8003d2e751de2913e5', 'society_admin', '1556222053ashraful-islam.jpg', 'sylhet', NULL, '01751381331', 'rumos@gmail.com', 'Bangladesh', 'Sylhet', 'Sylhet Sadar', '04-02-1993', '2019-04-25 19:54:13', '123456790', NULL, NULL, NULL, NULL, 'inactive', '2019-04-25 19:54:13', 1000, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cost`
--
ALTER TABLE `cost`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan`
--
ALTER TABLE `loan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member_share`
--
ALTER TABLE `member_share`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `savings`
--
ALTER TABLE `savings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `society`
--
ALTER TABLE `society`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `society_members`
--
ALTER TABLE `society_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cost`
--
ALTER TABLE `cost`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `loan`
--
ALTER TABLE `loan`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `member_share`
--
ALTER TABLE `member_share`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `savings`
--
ALTER TABLE `savings`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `society`
--
ALTER TABLE `society`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `society_members`
--
ALTER TABLE `society_members`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
