

<!-- Footer
================================================== -->
<div class="margin-top-15"></div>

<div id="footer">
	
	<!-- Bottom -->
	<div class="container">
		<div class="footer-bottom" style="margin-top:0;border-top:none">
			<div class="sixteen columns">
				<h4>Follow Us</h4>
				<ul class="social-icons">
					<li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
					<li><a class="gplus" href="#"><i class="icon-gplus"></i></a></li>
					<li><a class="linkedin" href="#"><i class="icon-linkedin"></i></a></li>
				</ul>
				<div class="copyrights">©  Copyright 2019 by <a href="#">Rumon Hussain</a>. All Rights Reserved.</div>
			</div>
		</div>
	</div>

</div>

<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>

</div>
<!-- Wrapper / End -->


<!-- Scripts
================================================== -->
<script src="<?php echo site_url(); ?>/assets/frontend/scripts/jquery-2.1.3.min.js"></script>
<script src="<?php echo site_url('assets/admin/'); ?>plugins/select2/select2.full.min.js"></script>
 <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script> 

<script>

  $.validate({
    modules : 'location, date, security, file',
    onModulesLoaded : function() {
      $('#country').suggestCountry();
    }
  });

  // Restrict presentation length
  $('#presentation').restrictLength( $('#pres-max-length') );
 $(".select2").select2();
</script>



</body>

<!-- Mirrored from www.vasterad.com/themes/workscout_1_5/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 Mar 2019 13:06:37 GMT -->
</html>