<?php $this->load->view('frontend/header'); ?>


<!-- Banner
================================================== -->
<div id="banner" class="with-transparent-header parallax background" style="background-image: url(<?php echo site_url(); ?>/assets/frontend/images/banner-home-02.jpg)" data-img-width="2000" data-img-height="1330" data-diff="300">
	<div class="container">
		<div class="sixteen columns">
			
			<div class="search-container">

				<!-- Form -->
				<h2>Find Society</h2>
				<input type="text" class="ico-01" placeholder="society or company name" value=""/>
				<button><i class="fa fa-search"></i></button>


			</div>

		</div>
	</div>
</div>


<!-- Content
================================================== -->

<!-- Categories -->
<div class="container">
	<div class="sixteen columns">
		<h3 class="margin-bottom-20 margin-top-10">Public Society</h3>

			<!-- Popular Categories -->
			<div class="categories-boxes-container">
				
				<!-- Box -->
				<?php if($all_socity){foreach($all_socity as $socity){ ?>
				<a href="browse-jobs.html" class="category-small-box">
					<i class="ln ln-icon-Bar-Chart"></i>
					<h4> <?php echo $socity->name; ?> </h4>
					<span> <?php echo $this->society->count_member($socity->id); ?> </span>
				</a>
                <?php }} ?>
				

			</div>

		<div class="clearfix"></div>
		<div class="margin-top-30"></div>

		<!--<a href="<?php echo site_url('societies'); ?>" class="button centered"> Browse All Societies</a>-->
		<div class="margin-bottom-55"></div>
	</div>
</div>







<?php $this->load->view('frontend/footer'); ?>