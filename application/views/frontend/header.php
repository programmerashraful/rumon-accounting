<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->

<!-- Mirrored from www.vasterad.com/themes/workscout_1_5/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 10 Mar 2019 13:06:05 GMT -->
<head>

<!-- Basic Page Needs
================================================== -->
<meta charset="utf-8">
<title>Home | <?php echo $this->user_model->get_setting_data('site_title'); ?> </title>

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo site_url(); ?>/assets/frontend/css/style.css">
<link rel="stylesheet" href="<?php echo site_url(); ?>/assets/frontend/css/custom.css">
<link rel="stylesheet" href="<?php echo site_url(); ?>/assets/frontend/css/colors/green.css" id="colors">

<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

</head>

<body>
<div id="wrapper">


<!-- Header
================================================== -->
<header class="transparent sticky-header">
<div class="container">
	<div class="sixteen columns">
	
		<!-- Logo -->
		<div id="logo">
			<h1><a href="<?php echo site_url(); ?>"><img src="<?php echo site_url(); ?>/assets/frontend/images/logo2.png" alt="Work Scout" /></a></h1>
		</div>

		<!-- Menu -->
		<nav id="navigation" class="menu">
			<ul id="responsive">

				<li><a href="<?php echo site_url(); ?>" id="current">Home</a></li>
			</ul>


			<ul class="float-right">
			
				<?php if($this->user_model->is_user_logd_in()){ ?>
				    <li><a href="<?php echo site_url('logout'); ?>"><i class="fa fa-user"></i> Logout</a></li>
				    <li><a target="_blank" href="<?php echo site_url('admin'); ?>"><i class="fa fa-lock"></i> Admin </a></li>
				<?php }else{ ?>
                    <li><a href="<?php echo site_url('signup/type'); ?>"><i class="fa fa-user"></i> Sign Up</a></li>
				    <li><a target="_blank" href="<?php echo site_url('login'); ?>"><i class="fa fa-lock"></i> Log In</a></li>
				<?php } ?>
			</ul>

		</nav>

		<!-- Navigation -->
		<div id="mobile-navigation">
			<a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i> Menu</a>
		</div>

	</div>
</div>
</header>
<div class="clearfix"></div>