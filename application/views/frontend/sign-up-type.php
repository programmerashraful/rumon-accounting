<?php $this->load->view('frontend/header'); ?>


<!-- Banner
================================================== -->
<div id="banner" class="with-transparent-header parallax background" style="background-image: url(<?php echo site_url(); ?>/assets/frontend/images/banner-home-02.jpg)" data-img-width="2000" data-img-height="1330" data-diff="300">
	<div class="container">
		<div class="sixteen columns">
			
			<div class="search-container">

				<!-- Form -->
				<h2>Sign Up</h2>
				


			</div>

		</div>
	</div>
</div>


<!-- Content
================================================== -->

<!-- Categories -->
<div class="container">
	<div class="sixteen columns">
			<!-- Popular Categories -->
			<div class="categories-boxes-container">
				
				<!-- Box -->
				<a href="<?php echo site_url('signup/society'); ?>" class="category-small-box">
					<i class="ln ln-icon-Bar-Chart"></i>
					<h4>I am a society</h4>
					
				</a>

				<!-- Box -->
				<a href="<?php echo site_url('signup/member'); ?>" class="category-small-box">
					<i class="ln  ln-icon-Worker"></i>
					<h4>I am a member</h4>
				</a>


			</div>

		<div class="clearfix"></div>
		<div class="margin-top-30"></div>

		<div class="margin-bottom-55"></div>
	</div>
</div>







<?php $this->load->view('frontend/footer'); ?>