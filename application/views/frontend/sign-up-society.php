<?php $this->load->view('frontend/header'); ?>


<!-- Banner
================================================== -->
<div id="banner" class="with-transparent-header parallax background" style="background-image: url(<?php echo site_url(); ?>/assets/frontend/images/banner-home-02.jpg)" data-img-width="2000" data-img-height="1330" data-diff="300">
	<div class="container">
		<div class="sixteen columns">
			
			<div class="search-container">

				<!-- Form -->
				<h2>Society Sign Up </h2>
				


			</div>

		</div>
	</div>
</div>


<!-- Content
================================================== -->

<!-- Categories -->
    
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 form_outer">
            
            
            
            <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
            
            
            
            
            <?php echo form_open_multipart('signup/society', array('id'=>'registration-form')); ?>
            <div class="row">
               <h3>Society details</h3>
                <div class="eight columns">
                    <div class="margin-top-15">
                        <label for="type">Society Type</label>
                        <select name="type" id="type" style="padding: 14px 18px;" data-validation="required" data-validation-error-msg="Society type is required">
                            <option value=""> Select... </option>
                            <option value="public" <?php if($this->input->post('type')=='public'){echo 'selected';} ?>  > Public </option>
                            <option value="private"  <?php if($this->input->post('type')=='private'){echo 'selected';} ?>  > Private </option>
                        </select>
                    </div>
                    
                    <div class="margin-top-15">
                        <label for="total_fund">Total Fund</label>
                        <input type="number" min="1" step="1" class="form-control" id="total_fund" placeholder="Society Name" name="total_fund" data-validation="required numeric" data-validation-error-msg="Total fund is required, and numeric" value="<?php echo $this->input->post('total_fund'); ?>">
                    </div>
                </div>
                <div class="eight columns">
                    <div class="margin-top-15">
                        <label for="society_name">Society Name</label>
                        <input type="text" class="form-control" id="society_name" placeholder="Society Name" name="society_name" data-validation="required" data-validation-error-msg="Society name is required" value="<?php echo $this->input->post('society_name'); ?>">
                    </div>
                    
                    
                    <div class="margin-top-15">
                        <label for="share_rate">Per share rate</label>
                        <input type="number" min="1" step="1" class="form-control" id="share_rate" placeholder="Share rate" name="share_rate" data-validation="required numeric" data-validation-error-msg="Share rate is required, and numeric" value="<?php echo $this->input->post('share_rate'); ?>">
                    </div>
                </div>
                <div class="sixteen columns">
                    <div class="margin-top-15">
                        <label for="society_description">Society Description</label>
                        <textarea name="society_description" id="society_description" cols="30" rows="3" data-validation="required" data-validation-error-msg="Society description is required" placeholder="Society Description"><?php echo $this->input->post('society_description'); ?></textarea>
                    </div>
                    
                </div>
            </div>
            <div class="row">
               <h3>Society admin user details</h3>
                <div class="eight columns">
                    <div class="margin-top-15">
                        <label for="first_name">First Name</label>
                        <input type="text" class="form-control" id="first_name" placeholder="First Name" name="first_name" data-validation="length alphanumeric" data-validation-length="3-12" data-validation-error-msg="User first name has to be an alphanumeric value (3-12 chars)" value="<?php echo $this->input->post('first_name'); ?>">
                    </div>
                    <div class="margin-top-15">
                        <label for="last_name">Last Name</label>
                        <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Last Name" data-validation="length alphanumeric" data-validation-length="3-12" data-validation-error-msg="User first name has to be an alphanumeric value (3-12 chars)" value="<?php echo $this->input->post('last_name'); ?>">
                    </div>
                    <div class="margin-top-15">
                        <label for="birthday">Birthday</label>
                        <input class="form-control" id="birthday" placeholder="Date of Birth" name="birthday" type="text" value="<?php echo $this->input->post('birthday'); ?>" data-validation="required" data-validation-error-msg="Date of birth is required">
                    </div>
                    <div class="margin-top-15">
                        <label for="user_name">User Name</label>
                        <input class="form-control" id="user_name" placeholder="User Name" name="user_name" type="text" value="" data-validation="length alphanumeric" data-validation-length="3-12" data-validation-error-msg="User first name has to be an alphanumeric value (3-12 chars)" value="<?php echo $this->input->post('user_name'); ?>">
                    </div>
                    <div class="margin-top-15">
                        <label for="email">User Email</label>
                        <input class="form-control" id="email" placeholder="User Email" name="email" type="email" value="<?php echo $this->input->post('email'); ?>"  data-validation="email">
                    </div>
                    <div class="margin-top-15">
                        <label for="phone">Moobile Number</label>
                        <input class="form-control" id="phone" placeholder="Moobile Number" name="phone" type="text" value="<?php echo $this->input->post('phone'); ?>" data-validation="length alphanumeric" data-validation-length="10-12" data-validation-error-msg="Phone number to be an alphanumeric value (10-12 chars)">
                    </div>
                    <div class="margin-top-15">
                        <label for="address1">Address</label>
                        <input class="form-control" id="address1" placeholder="Address" name="address1" type="text" value="<?php echo $this->input->post('address1'); ?>" data-validation="required" data-validation-error-msg="Address is required">
                    </div>
                </div>
                <div class="eight columns">
                    
                    <div class="margin-top-15">
                        <label for="country">Country</label>
                        <input class="form-control" id="country" placeholder="Country" name="country" type="text" value="<?php echo $this->input->post('country'); ?>" data-validation="required" data-validation-error-msg="Country is required">
                    </div>
                    <div class="margin-top-15">
                        <label for="city">District</label>
                        <input class="form-control" id="city" placeholder="District" name="city" type="text" value="<?php echo $this->input->post('city'); ?>" data-validation="required" data-validation-error-msg="District is required">
                    </div>
                    <div class="margin-top-15">
                        <label for="state">Upozilla</label>
                        <input class="form-control" id="state" placeholder="Upozilla" name="state" type="text" value="<?php echo $this->input->post('state'); ?>"  data-validation="required" data-validation-error-msg="Upozilla is required">
                    </div>
                    <div class="margin-top-15">
                        <label for="nid_number">NID Number</label>
                        <input class="form-control" id="nid_number" placeholder="NID number" name="nid_number" type="text" value="<?php echo $this->input->post('nid_number'); ?>" data-validation="required" data-validation-error-msg="NID Number is required">
                    </div>
                    <div class="margin-top-15">
                        <label for="password">Password</label>
                        <input type="password" id="password" name="pass_confirmation" placeholder="Password" data-validation="strength length" data-validation-strength="2" class="form-control" data-validation-length="min8" data-validation-error-msg="Password Must be eight character (a-z, A-Z, 0-9, Spacial symbol)">
                    </div>
                    <div class="margin-top-15">
                        <label for="re_password">Re password</label>
                        <input type="password" id="re_password" name="pass" placeholder="Re Password" data-validation="confirmation"  class="form-control">
                    </div>
                    <div class="margin-top-15">
                        <label for="user_image">Profile Picture (Max size 300kb)</label>
                        <input type="file" id="user_image" name="user_image" placeholder="Profile Picture"   class="form-control"  data-validation="mime size required" data-validation-allowing="jpg, png" data-validation-max-size="300kb" data-validation-error-msg-required="No image selected" style="padding:0px">
                        
                    </div>
                </div>
                <div class="sixteen columns">
                    <div class="margin-top-15">
                        <input type="submit" name="save_society" value="Submit" class="btn btn-success">
                    </div>
                </div>
            </div>
            
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
    
    






<?php $this->load->view('frontend/footer'); ?>