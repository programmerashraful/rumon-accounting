<?php $this->load->view('admin/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/inc/sidebar'); ?>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Cost List
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Cost List</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <?php if(isset($cost_by_id) and $cost_by_id){ ?>
               <form action="<?php echo site_url('admin/cost/index/'.$cost_by_id->id); ?>" method="post">
                   <div class="row">
                       <div class="col-sm-2">
                           <input type="number" value="<?php echo $cost_by_id->amount; ?>" min="1" name="cost_amount" placeholder="Cost Amount" class="form-control" required>
                       </div>
                       <div class="col-sm-4">
                           <input type="text" value="<?php echo $cost_by_id->comment; ?>" name="cost_comment" placeholder="Cost Comment" class="form-control">
                       </div>
                       <div class="col-sm-1">
                           <input name="cost_edit" type="submit" value="Save Cost" class="btn btn-success">
                       </div>
                   </div>
               </form>
               <?php }else{ ?>
               <form action="<?php echo site_url('admin/cost/'); ?>" method="post">
                   <div class="row">
                       <div class="col-sm-2">
                           <input type="number" value="<?php echo $this->input->get('cost_amount'); ?>" min="1" name="cost_amount" placeholder="Cost Amount" class="form-control" required>
                       </div>
                       <div class="col-sm-4">
                           <input type="text" value="<?php echo $this->input->get('cost_comment'); ?>" name="cost_comment" placeholder="Cost Comment" class="form-control">
                       </div>
                       <div class="col-sm-1">
                           <input name="cost_submit" type="submit" value="Save Cost" class="btn btn-success">
                       </div>
                   </div>
               </form>
               <?php } ?>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-sm-12">
                  <table class="table table-bordered">
                 <tbody><tr>
                  <th>Date</th>
                  <th>Comment</th>
                  <th>Amount</th>
                  
                  <th style="min-width: 179px;">Action</th>
                </tr>
                <?php if($cost_list): foreach($cost_list as $cost): ?>
                <tr>
                  
                  <td><?php echo $cost->date; ?></td>
                  <td><?php echo $cost->comment; ?></td>
                  <td><?php echo $cost->amount.$this->config->item('currency_symbol'); ?></td>
             
                  <td>
                    <div class="btn-group">
                        <a href="<?php echo site_url('admin/cost/index/'.$cost->id); ?>" class="btn btn-primary btn-sm">Edit Cost</a>
                        
                        <a onclick="return confirm('Do you want to delete the cost')" href="<?php echo site_url('admin/cost/delete/'.$cost->id); ?>" class="btn btn-danger btn-sm" >Delete</a>
                    </div>
                  </td>
                </tr>
                <?php endforeach; endif; ?>
                
              </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
 <?php $this->load->view('admin/inc/footer'); ?>