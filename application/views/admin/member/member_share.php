<?php $this->load->view('admin/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/inc/sidebar'); ?>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Member List <a href="<?php echo site_url('admin/member/add_new'); ?>" class="btn btn-success">Add new member</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Members List</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
               <form action="<?php echo site_url('admin/member/member_share/'); ?>" method="get">
                   <div class="row">
                       <div class="col-sm-4">
                           <input type="number" value="<?php echo $this->input->get('member_search_id'); ?>" min="1" name="member_search_id" id="" class="form-control">
                       </div>
                       <div class="col-sm-1">
                           <input type="submit" value="Search" class="btn btn-success">
                       </div>
                   </div>
               </form>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-sm-12">
                  <table class="table table-bordered">
                 <tbody><tr>
                  <th>Picture</th>
                  <th>Name</th>
                  <th>Member ID</th>
                  <th>Phone</th>
                  <th>Address</th>
                  <th>Total Share Number</th>
                  <th>Total Share Amount</th>
                  <th>Total savings</th>
                  <th style="min-width: 179px;">Action</th>
                </tr>
                <?php if($user_data): foreach($user_data as $user): ?>
                <tr>
                  <td style="max-width:100px"><a href="<?php echo site_url('admin/users/edit_user/'.$user->id); ?>"><img src="<?php echo site_url('uploads/users/'.$user->image); ?>" alt="" class="img-responsive" style="width:80px"></a></td>
                  <td><a href="<?php echo site_url('admin/users/edit_user/'.$user->id); ?>"><?php echo $user->first_name.' '.$user->last_name; ?></a></td>
                  <td><?php echo $user->id; ?></td>
                  <td><?php echo $user->phone; ?></td>
                  <td><?php echo $user->address1; ?></td>
                  <td><?php echo $this->member_model->member_share_number($user->id); ?></td>
                  <td><?php echo $this->member_model->member_share_amount($user->id).$this->config->item('currency_symbol'); ?></td>
                  <td><?php echo $this->member_model->member_savings($user->id).$this->config->item('currency_symbol'); ?></td>
                  <td>
                    <div class="btn-group">
                        <a href="<?php echo site_url('admin/member/details/'.$user->id); ?>" class="btn btn-primary btn-sm">Details</a>
                        <a href="<?php echo site_url('admin/member/share/'.$user->id); ?>" class="btn btn-success btn-sm">Share</a>
                    </div>
                    <div class="btn-group">
                        <a href="<?php echo site_url('admin/member/saving/'.$user->id); ?>" class="btn btn-danger btn-sm" >Saving</a>
                        <a href="<?php echo site_url('admin/member/loan/'.$user->id); ?>" class="btn btn-warning btn-sm" >Loan</a>
                    </div>
                  </td>
                </tr>
                <?php endforeach; endif; ?>
                
              </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
 <?php $this->load->view('admin/inc/footer'); ?>