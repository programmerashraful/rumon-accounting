<?php $this->load->view('admin/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/inc/sidebar'); ?>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Member Savings Information
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Members List</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border" style="background: #d2d6de;">
               <div class="col-sm-12">
                  <table class="table table-bordered ">
                 <tbody><tr>
                  <th>Picture</th>
                  <th>Name</th>
                  <th>Member ID</th>
                  <th>Phone</th>
                  <th>Email</th>
                  <th>Address</th>
                  <th>Total Share Number</th>
                  <th>Total Share Amount</th>
                  <th>Total Savings</th>
                </tr>
                <?php if($user_data): foreach($user_data as $user): ?>
                <tr>
                  <td style="max-width:50px"><a href="<?php echo site_url('admin/users/edit_user/'.$user->id); ?>"><img src="<?php echo site_url('uploads/users/'.$user->image); ?>" alt="" class="img-responsive img-thumbnail" style="width:80px"></a></td>
                  <td><a href="<?php echo site_url('admin/users/edit_user/'.$user->id); ?>" style="color:#000"><?php echo $user->first_name.' '.$user->last_name; ?></a></td>
                  <td><?php echo $user->id; ?></td>
                  <td><?php echo $user->phone; ?></td>
                  <td><?php echo $user->email; ?></td>
                  <td><?php echo $user->address1; ?></td>
                  <td><?php echo $this->member_model->member_share_number($user->id); ?></td>
                  <td><?php echo $this->member_model->member_share_amount($user->id); ?></td>
                  <td><?php echo $this->member_model->member_savings($user->id).$this->config->item('currency_symbol'); ?></td>
                 
                </tr>
                <?php endforeach; endif; ?>
                
              </tbody>
              </table>
              </div>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php if(!isset($savings_id)): ?>
              <form action="<?php echo site_url('admin/member/saving/'.$member_id); ?>" method="post">
                  <div class="col-sm-2">
                      <input type="number" min="<?php echo $this->user_model->get_setting_data('monthly_savings'); ?>" name="saving_amount" id="" class="form-control" placeholder="Saving Amount" required>
                  </div>
                  <div class="col-sm-3">
                      <select name="saving_month" id="" class="form-control" required>
                          <option value="">Select..</option>
                          <option value="1">January</option>
                          <option value="2">February</option>
                          <option value="3">March</option>
                          <option value="4">Appril</option>
                          <option value="5">May</option>
                          <option value="6">June</option>
                          <option value="7">July</option>
                          <option value="8">August</option>
                          <option value="9">September</option>
                          <option value="10">October</option>
                          <option value="11">November</option>
                          <option value="12">Decenber</option>
                      </select>
                  </div>
                  <div class="col-sm-2">
                      <input type="number" min="0" name="saving_year" id="" class="form-control" placeholder="Year" value="<?php echo date('Y',time()); ?>" required>
                  </div>
                  <div class="col-sm-3">
                      <input type="text"  name="ref_number" id="" class="form-control" placeholder="Ref-Number">
                  </div>
                  <div class="col-sm-1">
                      <input type="submit" name="save_new_saving" value="Submit" class="btn btn-success">
                  </div>
              </form>
              <?php endif;
                if(isset($savings_id)):
                ?>
              <form action="<?php echo site_url('admin/member/saving/'.$member_id.'/'.$savings_id); ?>" method="post">
                  <div class="col-sm-2">
                      <input type="number" min="0" name="saving_amount" id="" class="form-control" placeholder="Saving Amount" value="<?php echo $saving->saving_amount; ?>" required>
                  </div>
                  <div class="col-sm-3">
  <select name="saving_month" id="" class="form-control" required>
      <option value="">Select..</option>
      <option value="1" <?php if($saving->month==1){echo 'selected';} ?> >January</option>
      <option value="2"<?php if($saving->month==2){echo 'selected';} ?>>February</option>
      <option value="3"<?php if($saving->month==3){echo 'selected';} ?>>March</option>
      <option value="4"<?php if($saving->month==4){echo 'selected';} ?>>Appril</option>
      <option value="5"<?php if($saving->month==5){echo 'selected';} ?>>May</option>
      <option value="6"<?php if($saving->month==6){echo 'selected';} ?>>June</option>
      <option value="7"<?php if($saving->month==7){echo 'selected';} ?>>July</option>
      <option value="8"<?php if($saving->month==8){echo 'selected';} ?>>August</option>
      <option value="9"<?php if($saving->month==9){echo 'selected';} ?>>September</option>
      <option value="10"<?php if($saving->month==10){echo 'selected';} ?>>October</option>
      <option value="11"<?php if($saving->month==11){echo 'selected';} ?>>November</option>
      <option value="12"<?php if($saving->month==12){echo 'selected';} ?>>Decenber</option>
  </select>
                  </div>
                  <div class="col-sm-2">
                      <input type="number" min="0" name="saving_year" id="" class="form-control" placeholder="Year" value="<?php echo $saving->year; ?>" required>
                  </div>
                  <div class="col-sm-3">
                      <input type="text"  name="ref_number" id="" class="form-control" placeholder="Ref-Number" value="<?php echo $saving->ref_number; ?>">
                  </div>
                  <div class="col-sm-1">
                      <input type="submit" name="save_saving" value="Submit" class="btn btn-success">
                  </div>
              </form>
              <?php endif; ?>
              
              <div class="col-sm-12">
                  <table class="table table-bordered">
                      <thead>
                          <tr>
                              <th>Saving Amount</th>
                              <th>Saving For</th>
                              <th>Saving Time</th>
                              <th>Ref-Number</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>
                         <?php if($savings_list){foreach($savings_list as $saving){ ?>
                          <tr>
                              <td><?php if(!$saving->saving_amount){echo '<span class="badge badge-secondary" style="background-color:red">Not Paid</span>';}else{ echo $saving->saving_amount; }?></td>
                              <td><?php
                                        $monthNum = $saving->month;
                                     $monthName = date("F", mktime(0, 0, 0, $monthNum, 10));
                                     echo $monthName;                                            
                                echo ', '.$saving->year; ?></td>
                              <td><?php echo $saving->time; ?></td>
                              <td><?php echo $saving->ref_number; ?></td>
                              <td>
                                  <div class="btn-group">
                                        <a href="<?php echo site_url('admin/member/saving/'.$member_id.'/'.$saving->id); ?>" class="btn btn-success btn-sm"><i class="fa fa-pencil-square-o"></i></a>
                                        <a href="<?php echo site_url('admin/member/delete_member_saving/'.$saving->id.'/'.$member_id); ?>" class="btn btn-danger btn-sm" onclick="return confirm('Are you shure...? You really want delete this member');"><i class="fa fa-trash"></i></a>
                                    </div>
                              </td>
                          </tr>
                          <?php }} ?>
                          
                      </tbody>
                  </table>
              </div>
              
              
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
 <?php $this->load->view('admin/inc/footer'); ?>