<?php $this->load->view('admin/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/inc/sidebar'); ?>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Member Share Information
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Members List</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border" style="background: #d2d6de;">
               <div class="col-sm-12">
                  <table class="table table-bordered ">
                 <tbody><tr>
                  <th>Picture</th>
                  <th>Name</th>
                  <th>Member ID</th>
                  <th>Phone</th>
                  <th>Email</th>
                  <th>Address</th>
                  <th>Total Share Number</th>
                  <th>Total Share Amount</th>
                </tr>
                <?php if($user_data): foreach($user_data as $user): ?>
                <tr>
                  <td style="max-width:50px"><a href="<?php echo site_url('admin/users/edit_user/'.$user->id); ?>"><img src="<?php echo site_url('uploads/users/'.$user->image); ?>" alt="" class="img-responsive img-thumbnail" style="width:80px"></a></td>
                  <td><a href="<?php echo site_url('admin/users/edit_user/'.$user->id); ?>" style="color:#000"><?php echo $user->first_name.' '.$user->last_name; ?></a></td>
                  <td><?php echo $user->id; ?></td>
                  <td><?php echo $user->phone; ?></td>
                  <td><?php echo $user->email; ?></td>
                  <td><?php echo $user->address1; ?></td>
                  <td><?php echo $this->member_model->member_share_number($user->id); ?></td>
                  <td><?php echo $this->member_model->member_share_amount($user->id); ?></td>
                 
                </tr>
                <?php endforeach; endif; ?>
                
              </tbody>
              </table>
              </div>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php if(!isset($share_id)): ?>
              <form action="<?php echo site_url('admin/member/share/'.$member_id); ?>" method="post">
                  <div class="col-sm-2">
                      <input type="number" min="0" max="20" name="share_number" id="" class="form-control" placeholder="Share Number" required>
                  </div>
                  <div class="col-sm-2">
                      <input type="number" min="0" max="20000"  name="share_amount" id="" class="form-control"  placeholder="Share Amount" required>
                  </div>
                  <div class="col-sm-1">
                      <input type="submit" name="save_new_share" value="Submit" class="btn btn-success">
                  </div>
              </form>
              <?php endif;
                if(isset($share_id)):
                ?>
              <form action="<?php echo site_url('admin/member/share/'.$member_id.'/'.$share_id); ?>" method="post">
                  <div class="col-sm-2">
                      <input type="number" min="0" max="20"  name="share_number" id="" class="form-control" placeholder="Share Number" value="<?php echo $share->share_number; ?>" required>
                  </div>
                  <div class="col-sm-2">
                      <input type="number" min="0" max="20000"  name="share_amount" id="" class="form-control" value="<?php echo $share->share_amount; ?>" placeholder="Share Amount" required>
                  </div>
                  <div class="col-sm-1">
                      <input type="submit" name="save_share" value="Save" class="btn btn-success">
                  </div>
              </form>
              <?php endif; ?>
              
              <div class="col-sm-12">
                  <table class="table table-bordered">
                      <thead>
                          <tr>
                              <th>Share Number</th>
                              <th>Share Amount</th>
                              <th>Date</th>
                              <th>Action</th>
                          </tr>
                      </thead>
                      <tbody>
                         <?php if($share_list){foreach($share_list as $share){ ?>
                          <tr>
                              <td><?php echo $share->share_number; ?></td>
                              <td><?php echo $share->share_amount; ?></td>
                              <td><?php echo $share->date; ?></td>
                              <td>
                                  <div class="btn-group">
                                        <a href="<?php echo site_url('admin/member/share/'.$member_id.'/'.$share->id); ?>" class="btn btn-success btn-sm"><i class="fa fa-pencil-square-o"></i></a>
                                        <a href="<?php echo site_url('admin/member/delete_member_share/'.$share->id.'/'.$member_id); ?>" class="btn btn-danger btn-sm" onclick="return confirm('Are you shure...? You really want delete this member');"><i class="fa fa-trash"></i></a>
                                    </div>
                              </td>
                          </tr>
                          <?php }} ?>
                          
                      </tbody>
                  </table>
              </div>
              
              
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
 <?php $this->load->view('admin/inc/footer'); ?>