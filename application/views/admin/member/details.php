<?php $this->load->view('admin/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/inc/sidebar'); ?>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       <?php if($this->session->current_user_type!='admin'){ ?>
            Member Dashboard
       <?php }else{ ?>
           Member Total information
       <?php } ?>
       
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Members List</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <?php if($this->session->userdata('current_user_type')=='member'){ ?>
      <div class="row" style="margin-bottom:15px">
          <form action="<?php echo site_url('members/dashboard'); ?>" method="post">
            <div class="col-sm-3">
                <input type="text" name="user_name" placeholder="Member user name" class="form-control" value="<?php echo $this->input->post('user_name'); ?>">
              </div>
              <div class="col-sm-3">
                <input class="btn btn-success" type="submit" value="Search">
              </div>
          </form>
      </div>
      <?php } ?>
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border" style="background: #d2d6de;">
               <div class="col-sm-12">
                  <table class="table table-bordered ">
                 <tbody><tr>
                  <th>Picture</th>
                  <th>Name</th>
                  <th>Member ID</th>
                  <th>Phone</th>
                  <th>Email</th>
                  <th>Address</th>
                  <th>Total Share Number</th>
                  <th>Total Share Amount</th>
                  <th>Total Savings</th>
                  <th>Unpaid Loan</th>
                </tr>
                <?php if($user_data): foreach($user_data as $user): ?>
                <tr>
                  <td style="max-width:50px"><a href="<?php echo site_url('admin/users/edit_user/'.$user->id); ?>"><img src="<?php echo site_url('uploads/users/'.$user->image); ?>" alt="" class="img-responsive img-thumbnail" style="width:80px"></a></td>
                  <td><a href="<?php echo site_url('admin/users/edit_user/'.$user->id); ?>" style="color:#000"><?php echo $user->first_name.' '.$user->last_name; ?></a></td>
                  <td><?php echo $user->id; ?></td>
                  <td><?php echo $user->phone; ?></td>
                  <td><?php echo $user->email; ?></td>
                  <td><?php echo $user->address1; ?></td>
                  <td><?php echo $this->member_model->member_share_number($user->id); ?></td>
                  <td><?php echo $this->member_model->member_share_amount($user->id).$this->config->item('currency_symbol'); ?></td>
                  <td><?php echo $this->member_model->member_savings($user->id).$this->config->item('currency_symbol'); ?></td>
                  <td><?php echo $this->member_model->member_loans($user->id).$this->config->item('currency_symbol'); ?></td>
                 
                </tr>
                <?php endforeach; endif; ?>
                
              </tbody>
              </table>
              </div>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-sm-4 table-responsive" style="background: #D2D6DE;">
                 <h3>Share Details</h3>
                  <table class="table table-bordered"  style="background: #FFF">
                      <thead>
                          <tr>
                              <th>Share Number</th>
                              <th>Share Amount</th>
                              <th>Date</th>
                          </tr>
                      </thead>
                      <tbody>
                         <?php if($share_list){foreach($share_list as $share){ ?>
                          <tr>
                              <td><?php echo $share->share_number; ?></td>
                              <td><?php echo $share->share_amount; ?></td>
                              <td><?php echo $share->date; ?></td>
                             
                          </tr>
                          <?php }} ?>
                          
                      </tbody>
                  </table>
              </div>
              
              <div class="col-sm-4 table-responsive" style="background: #D2D6DE;">
                 <h3>Savings Details</h3>
                  <table class="table table-bordered"  style="background: #FFF">
                      <thead>
                          <tr>
                              <th>Saving Amount</th>
                              <th>Saving For</th>
                              <th>Ref Number</th>
                              <th>Saving Time</th>
                          </tr>
                      </thead>
                      <tbody>
                         <?php if($savings_list){foreach($savings_list as $saving){ ?>
                          <tr>
                              <td><?php if(!$saving->saving_amount){echo '<span class="badge badge-secondary" style="background-color:red">Not Paid</span>';}else{ echo $saving->saving_amount; }?></td>
                              <td><?php
                                        $monthNum = $saving->month;
                                     $monthName = date("F", mktime(0, 0, 0, $monthNum, 10));
                                     echo $monthName;                                            
                                echo ', '.$saving->year; ?></td>
                              <td><?php echo $saving->ref_number; ?></td>
                              <td><?php echo $saving->time; ?></td>
                       
                          </tr>
                          <?php }} ?>
                          
                      </tbody>
                  </table>
              </div>
              
              <div class="col-sm-4 table-responsive" style="background: #D2D6DE;">
                 <h3>Loan Details</h3>
                  <table class="table table-bordered"  style="background: #FFF">
                      <thead>
                          <tr>
                              <th>Amount</th>
                              <th>Return Amount</th>
                              <th>Sanction Time</th>
                          </tr>
                      </thead>
                      <tbody>
                         <?php if($loan_list){foreach($loan_list as $loan){ ?>
                          <tr>
                              <td><?php echo $loan->amount; ?></td>
                              <td><?php echo $loan->return_amount; ?></td>
                              <td><?php echo $loan->sanction_date; ?></td>
                          </tr>
                          <?php }} ?>
                          
                      </tbody>
                  </table>
              </div>
              
              
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
 <?php $this->load->view('admin/inc/footer'); ?>