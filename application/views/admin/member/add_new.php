<?php $this->load->view('admin/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/inc/sidebar'); ?>
 


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add new member 
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add new member</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Add new member</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             
						 
			<div class="col-sm-8">
			    <assaid class="customer_profile_info">
					<?php echo form_open('admin/member/add_new'); ?>
						<div class="form-group row">
							<label for="first_name" class="col-sm-3 col-form-label">  First Name <sup style="color:red"> &#9733; </sup> </label>
							<div class="col-sm-9">
								<input class="form-control" id="first_name" placeholder="First Name" name="first_name" type="text" value="<?php echo $this->input->post('first_name'); ?>" required>
							</div>
						</div>
					
						<div class="form-group row">
							<label for="last_name" class="col-sm-3 col-form-label">Last Name <sup style="color:red"> &#9733; </sup> </label>
							<div class="col-sm-9">
								<input class="form-control" id="last_name" placeholder="Last Name" name="last_name" type="text" value="<?php echo $this->input->post('last_name'); ?>" required>
							</div>
						</div>
						
						<div class="form-group row">
							<label for="birthday" class="col-sm-3 col-form-label">Date of Birth <sup style="color:red"> &#9733; </sup> </label>
							<div class="col-sm-9">
								<input class="form-control" id="birthday" placeholder="Date of Birth" name="birthday" type="text" value="<?php echo $this->input->post('birthday'); ?>" required>
							</div>
							
						</div>
					
						
						<div class="form-group row">
							<label for="nid_number" class="col-sm-3 col-form-label">NID Number</label>
							<div class="col-sm-9">
								<input class="form-control" id="nid_number" placeholder="NID number" name="nid_number" type="text" value="<?php echo $this->input->post('nid_number'); ?>">
							</div>
						</div>
						
						<div class="form-group row">
							<label for="address1" class="col-sm-3 col-form-label">Address <sup style="color:red"> &#9733; </sup> </label>
							<div class="col-sm-9">
								<input class="form-control" id="address1" placeholder="User address" name="address1" type="text" value="<?php echo $this->input->post('address1'); ?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label for="state" class="col-sm-3 col-form-label">Upazila <sup style="color:red"> &#9733; </sup> </label>
							<div class="col-sm-9">
								<input class="form-control" id="state" placeholder="Upazila" name="state" type="text" value="<?php echo $this->input->post('state'); ?>" required>
							</div>
						</div>

						<div class="form-group row">
							<label for="city" class="col-sm-3 col-form-label">District <sup style="color:red"> &#9733; </sup> </label>
							<div class="col-sm-9">
								<input class="form-control" id="city" placeholder="District" name="city" type="text" value="<?php echo $this->input->post('city'); ?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label for="country" class="col-sm-3 col-form-label">Country <sup style="color:red"> &#9733; </sup></label>
							<div class="col-sm-9">
								<input class="form-control" id="country" placeholder="Country" name="country" type="text" value="<?php echo $this->input->post('country'); ?>" required>
							</div>
						</div>
						<div class="form-group row">
							<label for="user_name" class="col-sm-3 col-form-label">User Name <sup style="color:red"> &#9733; </sup> </label>
							<div class="col-sm-9">
								<input class="form-control" id="user_name" placeholder="User Name" name="user_name" type="text" value="<?php echo $this->input->post('user_name'); ?>" required>
							</div>
						</div>
					
						<div class="form-group row">
							<label for="email" class="col-sm-3 col-form-label">Email  </label>
							<div class="col-sm-9">
								<input class="form-control" id="email" placeholder="User Email" name="email" type="email" value="<?php echo $this->input->post('email'); ?>">
							</div>
						</div>
						
						<div class="form-group row">
							<label for="password" class="col-sm-3 col-form-label">Password <sup style="color:red"> &#9733; </sup> </label>
							<div class="col-sm-9">
								<input class="form-control" id="password" placeholder="User Password" name="password" type="password" value="<?php echo $this->input->post('password'); ?>" required>
							</div>
						</div>
						
						<div class="form-group row">
							<label for="admission_fee" class="col-sm-3 col-form-label">Admission Fee </label>
							<div class="col-sm-9">
								<input class="form-control" id="admission_fee" placeholder="User Password" name="admission_fee" type="admission_fee" value="<?php echo $this->user_model->get_setting_data('member_admission_fee'); ?>" required disabled>
							</div>
						</div>
					
						<div class="form-group row">
							<label for="sign_up" class="col-sm-3 col-form-label">        </label>
							<div class="col-sm-9">
							    <input name="save_member" value="Save Member" class="btn btn-primary btn-reservation" type="submit">
							</div>
						</div>
						
					<?php echo form_close(); ?>
			    </assaid>
			</div>
           
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
 <?php $this->load->view('admin/inc/footer'); ?>