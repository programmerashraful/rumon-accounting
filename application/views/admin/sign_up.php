<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title> Sign Up | <?php echo $this->user_model->get_setting_data('site_title'); ?> </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/admin/'); ?>font-awesome/css/font-awesome.min.css" media="all" />
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="<?php echo site_url('assets/admin/'); ?>dist/css/custom.css">
</head>
<body class="sign_up_body">
    
    
    
    
    
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 form_outer">
            <div class="row">
                <div class="col-sm-12 text-center"><h1>SIGN UP FORM</h1></div>
            </div>
            
            
            
            <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
            
            
            
            
            <?php echo form_open_multipart('signup/index', array('id'=>'registration-form')); ?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input type="text" class="form-control" id="first_name" placeholder="First Name" name="first_name" data-validation="length alphanumeric" data-validation-length="3-12" data-validation-error-msg="User first name has to be an alphanumeric value (3-12 chars)" value="<?php echo $this->input->post('first_name'); ?>">
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Last Name" data-validation="length alphanumeric" data-validation-length="3-12" data-validation-error-msg="User first name has to be an alphanumeric value (3-12 chars)" value="<?php echo $this->input->post('last_name'); ?>">
                    </div>
                    <div class="form-group">
                        <label for="birthday">Birthday</label>
                        <input class="form-control" id="birthday" placeholder="Date of Birth" name="birthday" type="text" value="<?php echo $this->input->post('birthday'); ?>" data-validation="required" data-validation-error-msg="Date of birth is required">
                    </div>
                    <div class="form-group">
                        <label for="user_name">User Name</label>
                        <input class="form-control" id="user_name" placeholder="User Name" name="user_name" type="text" value="" data-validation="length alphanumeric" data-validation-length="3-12" data-validation-error-msg="User first name has to be an alphanumeric value (3-12 chars)" value="<?php echo $this->input->post('user_name'); ?>">
                    </div>
                    <div class="form-group">
                        <label for="email">User Email</label>
                        <input class="form-control" id="email" placeholder="User Email" name="email" type="email" value="<?php echo $this->input->post('email'); ?>"  data-validation="email">
                    </div>
                    <div class="form-group">
                        <label for="phone">Moobile Number</label>
                        <input class="form-control" id="phone" placeholder="Moobile Number" name="phone" type="text" value="<?php echo $this->input->post('phone'); ?>" data-validation="length alphanumeric" data-validation-length="10-12" data-validation-error-msg="Phone number to be an alphanumeric value (10-12 chars)">
                    </div>
                    <div class="form-group">
                        <label for="address1">Address</label>
                        <input class="form-control" id="address1" placeholder="Address" name="address1" type="text" value="<?php echo $this->input->post('address1'); ?>" data-validation="required" data-validation-error-msg="Address is required">
                    </div>
                </div>
                <div class="col-sm-6">
                    
                    <div class="form-group">
                        <label for="country">Country</label>
                        <input class="form-control" id="country" placeholder="Country" name="country" type="text" value="<?php echo $this->input->post('country'); ?>" data-validation="required" data-validation-error-msg="Country is required">
                    </div>
                    <div class="form-group">
                        <label for="city">District</label>
                        <input class="form-control" id="city" placeholder="District" name="city" type="text" value="<?php echo $this->input->post('city'); ?>" data-validation="required" data-validation-error-msg="District is required">
                    </div>
                    <div class="form-group">
                        <label for="state">Upozilla</label>
                        <input class="form-control" id="state" placeholder="Upozilla" name="state" type="text" value="<?php echo $this->input->post('state'); ?>"  data-validation="required" data-validation-error-msg="Upozilla is required">
                    </div>
                    <div class="form-group">
                        <label for="nid_number">NID Number</label>
                        <input class="form-control" id="nid_number" placeholder="NID number" name="nid_number" type="text" value="<?php echo $this->input->post('nid_number'); ?>" data-validation="required" data-validation-error-msg="NID Number is required">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" id="password" name="pass_confirmation" placeholder="Password" data-validation="strength" data-validation-strength="2" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="re_password">Re password</label>
                        <input type="password" id="re_password" name="pass" placeholder="Re Password" data-validation="confirmation"  class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="user_image">Profile Picture</label>
                        <input type="file" id="user_image" name="user_image" placeholder="Profile Picture"   class="form-control"  data-validation="mime size required" data-validation-allowing="jpg, png" data-validation-max-size="300kb" data-validation-error-msg-required="No image selected" style="padding:0px">
                        
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <input type="submit" name="save_member" value="Submit" class="btn btn-success">
                    </div>
                </div>
            </div>
            
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
    
    
    
    
    
    
    
    
    
    <script src="<?php echo site_url('assets/admin/'); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>

<script>
   $(document).ready(function(){
       $("#search_data").keyup(function(){
            var ajax_url = $(this).attr('ajax-action');
            var site_url = $('#site_url').val();
           $('.loading_div').removeClass('loading_dactive');
           $('.loading_div').addClass('loading_active');
            var data_of_q = $(this).val();
            var formData = new FormData();
            formData.append('search', data_of_q);
            $.ajax({
                url: ajax_url,
                dataType: 'json',
                type: 'post',
                data: formData,
                processData: false,
                contentType: false,
                success: function(data){
                    setTimeout(function() {
                        var row=null;
                        for (var single_data in data){
                             row +='<tr><td style="max-width:100px"><a href="'+site_url+'admin/users/edit_user/'+data[single_data]['id']+'"><img src="'+site_url+'uploads/users/'+data[single_data]['image']+'" alt="" class="img-responsive" style="width:80px"></a></td> <td><a href="'+site_url+'admin/users/edit_user/'+data[single_data]['id']+'">'+data[single_data]['first_name']+' '+data[single_data]['last_name']+'</a></td> <td>'+data[single_data]['user_name']+'</td><td>'+data[single_data]['type']+'</td><td>+'+data[single_data]['phone']+'</td><td>'+data[single_data]['email']+'</td><td>'+data[single_data]['country']+'</td><td><div class="btn-group"><a target="_blank" href="http://localhost/hotel-management/user/profile/'+data[single_data]['user_name']+'" class="btn btn-success"><i class="fa fa-eye"></i></a> <a href="'+site_url+'admin/menual_book/search_room/'+data[single_data]['id']+'" class="btn btn-primary"><i class="fa fa-check"></i></a> </div> </td></tr>';
                            
                        }
                        
                        //data print method
                        $('.usert_data_table').html(row);
                        
                        //desable loading bar
                        $('.loading_div').removeClass('loading_active');
                        $('.loading_div').addClass('loading_dactive');
                        
                    }, 1500);

                },

            });
        });
      
   });
</script>

<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo site_url('assets/admin/'); ?>bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo site_url('assets/admin/'); ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo site_url('assets/admin/'); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#tableexample1").DataTable();

  });
</script>

<!-- Sparkline -->
<script src="<?php echo site_url('assets/admin/'); ?>plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo site_url('assets/admin/'); ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo site_url('assets/admin/'); ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo site_url('assets/admin/'); ?>plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo site_url('assets/admin/'); ?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo site_url('assets/admin/'); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script>
        $('#checkindate').datepicker({});
		$('#checkoutdate').datepicker({});
		$('#birthday').datepicker({
            endDate: '-18y',
            startDate: '-150y',
        });
		$('#car_check_in').datepicker({});
		$('#model_checkindate').datepicker({});
		$('#model_checkoutdate').datepicker({});
</script>
<script src="<?php echo site_url('assets/admin/'); ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo site_url('assets/admin/'); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo site_url('assets/admin/'); ?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo site_url('assets/admin/'); ?>dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="<?php echo site_url('assets/admin/'); ?>dist/js/demo.js"></script>
<script src="<?php echo site_url('assets/admin/'); ?>dist/js/jquery.form-validator.min.js"></script>

<script>

  $.validate({
    modules : 'location, date, security, file',
    onModulesLoaded : function() {
      $('#country').suggestCountry();
    }
  });

  // Restrict presentation length
  $('#presentation').restrictLength( $('#pres-max-length') );

</script>


</body>
</html>
