<?php 
$user_data = $this->user_model->get_user($this->session->userdata('current_user_id'));
$user_type = $this->session->userdata('current_user_type');

?>
  
<?php 

$template = $this->uri->segment(2);
$submenu = $this->uri->segment(3);
$third_menu = $this->uri->segment(4);
$dashboard = '';

$users = '';
$all_users = '';
$add_new_user = '';

// auto post social media
$members = '';
$all_member = '';
$add_new_member = '';
$member_share = '';

//cost controllers
$cost='';



//this variable for members
$settings ='';
$member_settings ='';


if($template=='users'){
  $users='active';   
}
elseif($template=='dashboard'){
  $dashboard='active';   
}
elseif($template=='settings'){
  $settings='active';   
}
elseif($template=='member'){
  $members='active';   
}

elseif($template=='cost'){
  $cost='active';   
}



/*Sub menu handalling*/

if($submenu=='all_user' or $submenu=='edit_user'){
  $all_users='active';   
}
if($submenu=='add_new'){
  $add_new_user='active';   
}

if($template=='member' and $submenu=='all_member'){
   $all_member ='active'; 
}

if($template=='member' and $submenu=='add_new'){
   $add_new_member ='active'; 
}

if($template=='member' and $submenu=='member_share'){
   $member_share ='active'; 
}



if($template=='settings' and $submenu=='member'){
   $member_settings ='active'; 
}




?>
 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo site_url('uploads/users/'.$user_data->image); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $user_data->first_name.' '.$user_data->last_name; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
     
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
         
        <li class="<?php echo $dashboard; ?> treeview">
          <a href="<?php echo site_url('admin/dashboard'); ?>">
            <i class="fa fa-dashboard text-yellow"></i> <span>Dashboard</span>
          </a>
        </li>
        
        <?php if($this->session->userdata('current_user_type')=='admin'): ?>
        <li class="treeview <?php echo $members; ?>">
          <a href="#">
            <i class="fa fa-users"></i>
            <span> Members </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
           <li class="<?php echo $all_member; ?>"><a href="<?php echo site_url('admin/member/all_member'); ?>"><i class="fa fa-list"></i> All Member</a></li>
            
            <li class="<?php echo $add_new_member; ?>"><a href="<?php echo site_url('admin/member/add_new'); ?>"><i class="fa fa fa-plus-circle"></i> Add New Member</a></li>
            
            <li class="<?php echo $member_share; ?>"><a href="<?php echo site_url('admin/member/member_share'); ?>"><i class="fa  fa-share-alt"></i> Member Info</a></li>
            
           
            
          </ul>
        </li>
        
        <li class="<?php echo $cost; ?> treeview">
          <a href="<?php echo site_url('admin/cost'); ?>">
            <i class="fa fa-money "></i> <span>Cost</span>
          </a>
        </li>
        
        <li class="treeview <?php echo $settings; ?>">
          <a href="#">
            <i class="fa fa-cogs"></i>
            <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo $member_settings; ?>">
              <a href="<?php echo site_url('admin/settings/member'); ?>"><i class="fa fa-users"></i> Member Settings
              </a>
            </li>
            
          </ul>
        </li>

        
        
       
        <li class="treeview <?php echo $users; ?>">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Admin</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo $all_users; ?>"><a href="<?php echo site_url('admin/users/all_user'); ?>"><i class="fa fa-list"></i> All admin</a></li>
            
            <li class="<?php echo $add_new_user; ?>"><a href="<?php echo site_url('admin/users/add_new'); ?>"><i class="fa fa-plus-circle"></i>Add New Admin</a></li>
            
          </ul>
        </li>
       <?php endif; ?>
        
        
        <li class="treeview"><a href="<?php echo site_url('logout'); ?>"><i class="fa fa-power-off text-yellow"></i> <span>Log Out</span></a></li>
        
      </ul>
     
    </section>
    <!-- /.sidebar -->
  </aside>