 <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      
    </div>
    <strong>Copyright &copy; 2019</strong> All rights
    reserved.
  </footer>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo site_url('assets/admin/'); ?>plugins/jQuery/jquery-2.2.3.min.js"></script>

<script>
   $(document).ready(function(){
       $("#search_data").keyup(function(){
            var ajax_url = $(this).attr('ajax-action');
            var site_url = $('#site_url').val();
           $('.loading_div').removeClass('loading_dactive');
           $('.loading_div').addClass('loading_active');
            var data_of_q = $(this).val();
            var formData = new FormData();
            formData.append('search', data_of_q);
            $.ajax({
                url: ajax_url,
                dataType: 'json',
                type: 'post',
                data: formData,
                processData: false,
                contentType: false,
                success: function(data){
                    setTimeout(function() {
                        var row=null;
                        for (var single_data in data){
                             row +='<tr><td style="max-width:100px"><a href="'+site_url+'admin/users/edit_user/'+data[single_data]['id']+'"><img src="'+site_url+'uploads/users/'+data[single_data]['image']+'" alt="" class="img-responsive" style="width:80px"></a></td> <td><a href="'+site_url+'admin/users/edit_user/'+data[single_data]['id']+'">'+data[single_data]['first_name']+' '+data[single_data]['last_name']+'</a></td> <td>'+data[single_data]['user_name']+'</td><td>'+data[single_data]['type']+'</td><td>+'+data[single_data]['phone']+'</td><td>'+data[single_data]['email']+'</td><td>'+data[single_data]['country']+'</td><td><div class="btn-group"><a target="_blank" href="http://localhost/hotel-management/user/profile/'+data[single_data]['user_name']+'" class="btn btn-success"><i class="fa fa-eye"></i></a> <a href="'+site_url+'admin/menual_book/search_room/'+data[single_data]['id']+'" class="btn btn-primary"><i class="fa fa-check"></i></a> </div> </td></tr>';
                            
                        }
                        
                        //data print method
                        $('.usert_data_table').html(row);
                        
                        //desable loading bar
                        $('.loading_div').removeClass('loading_active');
                        $('.loading_div').addClass('loading_dactive');
                        
                    }, 1500);

                },

            });
        });
      
   });
</script>

<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo site_url('assets/admin/'); ?>bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo site_url('assets/admin/'); ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo site_url('assets/admin/'); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $("#tableexample1").DataTable();

  });
</script>

<!-- Sparkline -->
<script src="<?php echo site_url('assets/admin/'); ?>plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo site_url('assets/admin/'); ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo site_url('assets/admin/'); ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo site_url('assets/admin/'); ?>plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?php echo site_url('assets/admin/'); ?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo site_url('assets/admin/'); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script>
        $('#checkindate').datepicker({});
		$('#checkoutdate').datepicker({});
		$('#birthday').datepicker({
            endDate: '-18y',
            startDate: '-150y',
        });
		$('#car_check_in').datepicker({});
		$('#model_checkindate').datepicker({});
		$('#model_checkoutdate').datepicker({});
</script>
<script src="<?php echo site_url('assets/admin/'); ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo site_url('assets/admin/'); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo site_url('assets/admin/'); ?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo site_url('assets/admin/'); ?>dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="<?php echo site_url('assets/admin/'); ?>dist/js/demo.js"></script>

<script>

    $('#add_account_type').change(function(){
    var id= $(this).val();
    $.ajax({
            url: '<?php echo site_url('admin/account/account_category_by_type/'); ?>'+id,
            success: function(result){
                $('.account_cat_option').html(result);
            }
        });
	});
    $('#edit_account_type').change(function(){
    var id= $(this).val();
    $.ajax({
            url: '<?php echo site_url('admin/account/account_category_by_type/'); ?>'+id,
            success: function(result){
                $('.edit_account_cat_option').html(result);
            }
        });
	});
    $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
    $('#datepicker').datepicker();
    $('#datepicker_edit').datepicker();
</script>


<script>
   $(document).ready(function(){
        var sortlist = $('tbody#sortable');
        var ajaxurl = $('#site_url').val();
        ajaxurl+='admin/staff/update_sorting/';
       sortlist.sortable({
           update: function(event, ui){
               $.ajax({
                   url: ajaxurl,
                   type: 'POST',
                   data: {
                       orders: sortlist.sortable( 'toArray' )
                   },
                   success: function(data){
                       $('#message_section_1').html('<div class="col-sm-12 message_display_class"><div class="alert alert-success alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong>Your sorting update successfully</div></div>');
                   },
                   errors: function(errors){
                    $('#message_section_1').html('<div class="col-sm-12 message_display_class"><div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong>Somthing wrong please try again</div></div>');
                    }
               });
           }
       });
    });
</script>


</body>
</html>
