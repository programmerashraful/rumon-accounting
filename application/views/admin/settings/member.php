<?php $this->load->view('admin/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/inc/sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        MEMBER SETITNGS
      </h1>
     
    </section>
    <!-- Main content -->
    <section class="content">
     
     
     <!--this is error or success message display message-->
     <div class="row" id="message_section">
		    <!--Display the confirmation message -->
            <?php if($this->session->userdata('success_msg') or $this->session->userdata('error_msg')): ?>
			<div class="col-sm-12 message_display_class">
                <?php if($this->session->userdata('success_msg')): ?>
				<div class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				  <strong>Success!</strong> <?php echo $this->session->userdata('success_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if($this->session->userdata('error_msg')): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $this->session->userdata('error_msg'); ?>
				</div>
                <?php endif; ?>
                <?php if(isset($validation_errors)): ?>
				<div class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				 <strong>Faield!</strong> <?php echo $validation_errors; ?>
				</div>
                <?php endif; ?>
                <?php  $sesattr = array('success_msg' => '', 'error_msg' => '' );
       $this->session->set_userdata($sesattr); ?>
			</div>
            <?php endif; ?>
		</div>
     <!--this is error or success message display message-->
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
      <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">MEMBER SETTINGS</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             
			<div class="col-sm-8">
			    <assaid class="customer_profile_info">
					<?php echo form_open('admin/settings/member'); ?>
			
					
					
						<div class="form-group row">
							<label for="member_admission_fee" class="col-sm-3 col-form-label">ADMISSION FEE</label>
							<div class="col-sm-9">
                               
                                <div class="input-group">
                                   <span class="input-group-addon"><?php echo config_item('currency_symbol');?></span>
                                    <input class="form-control" id="member_admission_fee" placeholder="MEMBER ADMISSION FEE" name="member_admission_fee" type="text" value="<?php if($this->user_model->get_setting_data('member_admission_fee')){echo $this->user_model->get_setting_data('member_admission_fee');} ?>" required>
                                </div>
                                
                                
							</div>
						</div>
					
						<div class="form-group row">
							<label for="member_admission_fee" class="col-sm-3 col-form-label">MONTHLY SAVINGS</label>
							<div class="col-sm-9">
                               
                                <div class="input-group">
                                   <span class="input-group-addon"><?php echo config_item('currency_symbol');?></span>
                                    <input class="form-control" id="monthly_savings" placeholder="MONTHLY SAVINGS" name="monthly_savings" type="text" value="<?php if($this->user_model->get_setting_data('monthly_savings')){echo $this->user_model->get_setting_data('monthly_savings');} ?>" required>
                                </div>
                                
                                
							</div>
						</div>
					
						<div class="form-group row">
							<label for="share_price" class="col-sm-3 col-form-label">SHARE PRICE</label>
							<div class="col-sm-9">
                                <div class="input-group">
                                   <span class="input-group-addon"><?php echo config_item('currency_symbol');?></span>
                                    <input class="form-control" id="share_price" placeholder="SHARE PRICE" name="share_price" type="text" value="<?php if($this->user_model->get_setting_data('share_price')){echo $this->user_model->get_setting_data('share_price');} ?>" required>
                                </div>
							</div>
						</div>
					
						<div class="form-group row">
							<label for="total_share" class="col-sm-3 col-form-label">TOTAL SHARE</label>
							<div class="col-sm-9">
                                <div class="input-group" style="width:100%">
                                    <input class="form-control"  id="total_share" placeholder="TOTAL SHARE" name="total_share" type="text" value="<?php if($this->user_model->get_setting_data('total_share')){echo $this->user_model->get_setting_data('total_share');} ?>" required>
                                </div>
							</div>
						</div>
					
					
						<div class="form-group row">
							<label for="sign_up" class="col-sm-3 col-form-label">        </label>
							<div class="col-sm-9">
							    <input name="save_settings" value="Save Settings" class="btn btn-primary btn-reservation" type="submit">
							</div>
						</div>
						
					<?php echo form_close(); ?>
			    </assaid>
			</div>
             
      
              
           
            </div>
          </div>
        </div>
       
       
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
 <?php $this->load->view('admin/inc/footer'); ?>