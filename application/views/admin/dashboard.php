<?php $this->load->view('admin/inc/header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
<?php $this->load->view('admin/inc/sidebar'); ?>
<?php 
$user_data = $this->user_model->get_user($this->session->userdata('current_user_id'));
$total_message_count = 0;
$total_member = $this->user_model->get_user_count('member');





//Total mother account income
$total_mother_account_balance = 0;
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> <?php $this->lang->line('hoe'); ?></a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
     
     
      <!-- Small boxes (Stat box) -->
      <div class="row">
        
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>Total Members</h3>

              <h3><?php echo $total_member; ?></h3>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
                <a href="<?php echo site_url('admin/member/member_share'); ?>" class="small-box-footer"> Information <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>Total Shares</h3>

             <h3><?php echo $this->member_model->member_share_number(); ?></h3>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
               <a href="<?php echo site_url('admin/member/member_share'); ?>" class="small-box-footer"> Information <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>Total Share  Amount</h3>

              <h3><?php echo $this->member_model->member_share_amount().$this->config->item('currency_symbol'); ?></h3>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
                <a href="<?php echo site_url('admin/member/member_share'); ?>" class="small-box-footer"> Information <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>Total Savings</h3>

              <h3><?php echo $this->member_model->member_savings().$this->config->item('currency_symbol'); ?></h3>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
            <a href="<?php echo site_url('admin/member/member_share'); ?>" class="small-box-footer"> Information <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
     
      
      <div class="row">
          
          
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>Total Admission Fee</h3>

              <h3><?php echo $this->member_model->admission_fees().$this->config->item('currency_symbol'); ?></h3>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
                <a href="<?php echo site_url('admin/member/member_share'); ?>" class="small-box-footer"> Information <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
          
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>Total Cost</h3>

             <h3><?php echo $this->cost_model->total_cost(); echo $this->config->item('currency_symbol'); ?></h3>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
               <a href="<?php echo site_url('admin/cost/'); ?>" class="small-box-footer"> Information <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
          
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>Total Unpaid Loan</h3>

             <h3><?php echo $this->member_model->member_loans().$this->config->item('currency_symbol'); ?></h3>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
               <a href="<?php echo site_url('admin/cost/'); ?>" class="small-box-footer"> Information <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
          
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>Total Loan Interest</h3>

             <h3><?php echo $this->member_model->member_loan_profit().$this->config->item('currency_symbol'); ?></h3>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
               <a href="<?php echo site_url('admin/cost/'); ?>" class="small-box-footer"> Information <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
          
           <div class="col-lg-6 col-xs-6 col-lg-offset-3">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>Total Balance</h3>

              <h3><?php echo $this->member_model->member_savings()+$this->member_model->member_share_amount()+$this->member_model->admission_fees()-$this->cost_model->total_cost()-$this->member_model->member_loans()+$this->member_model->member_loan_profit(); echo $this->config->item('currency_symbol'); ?></h3>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
            <a href="<?php echo site_url('admin/member/member_share'); ?>" class="small-box-footer"> Information <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
 <?php $this->load->view('admin/inc/footer'); ?>