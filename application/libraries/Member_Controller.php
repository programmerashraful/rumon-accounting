<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_Controller extends CI_Controller {
    function __construct(){
        parent::__construct();
        
        $this->load->model('user_model');
        if(!$this->user_model->is_user_logd_in()){
             redirect('login');
        }
       
    }
}
