<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cost extends Admin_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('cost_model');
        
    }
	public function index($cost_id=null)
	{
        // save new cost
        if($this->input->post('cost_submit')){
            if($this->db->insert('cost', array('amount'=>$this->input->post('cost_amount'), 'comment'=>$this->input->post('cost_comment')))){
                $this->session->set_userdata('success_msg', 'Cost Add success');
            }else{
                $this->session->set_userdata('error_msg', 'Something Wrong please try again');
            }
        }
        
        // edit cost
        if($cost_id){
            $data['cost_id']=$cost_id;
            $data['cost_by_id']=$this->cost_model->cost_by_id($cost_id);
        }
        if($this->input->post('cost_edit')){
            if($this->db->where('id', $cost_id)->update('cost', array('amount'=>$this->input->post('cost_amount'), 'comment'=>$this->input->post('cost_comment')))){
                $this->session->set_userdata('success_msg', 'Cost Edit success');
                redirect('admin/cost');
            }else{
                $this->session->set_userdata('error_msg', 'Something Wrong please try again');
            }
        }
        
        $data['cost_list']=$this->cost_model->cost_list();
		 $this->load->view('admin/cost/cost_list', $data);
	}
    
    
    
    // function for delete cost
    public function delete($id){
        if($this->db->where('id', $id)->delete('cost')){
            $this->session->set_userdata('success_msg', 'You have successfully delete a cost');
            redirect('admin/cost');
        }else{
            $this->session->set_userdata('error_msg', 'Something went wrong please try again letter');
            redirect('admin/cost');
        }
    }
    
    
    
    
    
}

