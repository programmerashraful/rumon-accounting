<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Admin_Controller{
    public function __construct(){
        parent::__construct();
        if($this->session->userdata('current_user_type')!='admin'){redirect('admin/dashboard');}
        
    }
	public function index()
	{
        echo 'Page not found';
	}
    
    
    public function member(){
        if($this->input->post('save_settings')){
            $attr=array(
                'member_admission_fee'=>$this->input->post('member_admission_fee'),
                'monthly_savings'=>$this->input->post('monthly_savings'),
                'share_price'=>$this->input->post('share_price'),
                'total_share'=>$this->input->post('total_share')
            );
            if($this->update_settings_data($attr)){
                $this->session->set_userdata('success_msg', 'Your settings has been updated');
            }else{
                $this->session->set_userdata('success_msg', 'Something wrong please try again');
            }
        }
        
        
        $this->load->view('admin/settings/member');
    }
    
    
    
    
    
    
    
    // This method for update settings data 
    public function update_settings_data($attr){
        if($attr){
            $errors=array();
            foreach($attr as $key=>$data){
                if(!empty($data)){
                    if($this->user_model->get_setting_data($key)){
                        if($this->db->where('data_id', $key)->update('setting', array('data_id'=>$key, 'data'=>$data))){
                            //nothing 
                        }
                    }else{
                        if($this->db->insert('setting', array('data_id'=>$key, 'data'=>$data))){
                            // nothing
                        }else{
                            $error[$key]=$key.' is not updated';
                        }
                    }
                }
            }
            if($errors){
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }
    
    
    
    /*This is a fucntion that upload the hotel image*/
    public function do_upload($upload_path, $file_input_field){
        $path = $upload_path; 
        $fileTmpName=$_FILES[$file_input_field]["tmp_name"];
        $upload_dir = $upload_path; 
        $file_name = $_FILES[$file_input_field]['name'];
        $type = explode('.', $file_name);
        $type = $type[count($type)-1];
        $time = time();
        if( in_array($type, array('jpg', 'png', 'jpeg', 'gif', 'JPEG', 'PNG', 'JPG', 'GIF' )) ){
            if( is_uploaded_file( $_FILES[$file_input_field]['tmp_name'] ) ){
                move_uploaded_file( $_FILES[$file_input_field]['tmp_name'], $upload_dir.$time.$file_input_field.$file_name );
                return $time.$file_input_field.$file_name;
            }
        }else{
            $this->session->set_userdata('error_msg', 'File type not supported');
            return false;
        }
   }
    
    
    
}

