<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends Admin_Controller{
    public function __construct(){
        parent::__construct();
        $this->user_types = array('admin', 'member');
        $this->load->model('member_model');
    }
	public function index()
	{ 
        $this->all_member('member');
	}
    
    // Load all member
    
    public function all_member($user_type='member', $page=null)
	{
        
        $this->load->library('pagination');
        $config['base_url'] = base_url().'admin/member/all_member/';
        $config['use_page_numbers'] = FALSE;
        $config['total_rows'] = $this->user_model->get_user_count($user_type);
		$config['per_page'] = 10;
        $config['full_tag_open'] = '<ul style="margin: 0;" class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['last_link'] = 'LAST →';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['first_link'] = '← FIRST';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<a>';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '';
		$config['prev_link'] = '';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
        if(in_array($user_type, $this->user_types)){
        $config['base_url'] = base_url().'admin/member/all_member/';
            $config['base_url'] = base_url().'admin/member/all_member/'.$user_type;
           $current_page=$this->uri->segment(5);
        }else{
            $current_page=$current_page=$this->uri->segment(4);
            $user_type=null;
            $config['total_rows'] = $this->user_model->get_user_count();
             $config['base_url'] = base_url().'admin/member/all_member/';
        }
        
        $this->pagination->initialize($config); 
        $data['links']=$this->pagination->create_links();
        $data['user_type']=$user_type;
        $data['user_types']=$this->user_types;
        $data['user_data']=$this->user_model->get_all_user($user_type, $config['per_page'],  $current_page);
		$this->load->view("admin/member/all_member", $data);
	}
    
    // add new member
    public function add_new(){
        $data=array();
         if($this->input->post('save_member')){
             
             $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
             $this->form_validation->set_rules('user_name', 'User Name', 'trim|required|is_unique[users.user_name]');
             
            $update_attr=array(
                'first_name'=>$this->input->post('first_name'),
                'last_name'=>$this->input->post('last_name'),
                'user_name'=>$this->input->post('user_name'),
                'birthday'=>$this->input->post('birthday'),
                'address1'=>$this->input->post('address1'),
                'city'=>$this->input->post('city'),
                'state'=>$this->input->post('state'),
                'password'=>md5($this->input->post('password')),
                'image'=>'demo-avater.png',
                'email'=>$this->input->post('email'),
                'admission_fee'=>$this->input->post('admission_fee'),
                'type'=>'member'
            );
            
             if($this->form_validation->run()){
                   if($this->db->insert('users', $update_attr)){
                    $this->session->set_userdata('success_msg', 'You have successfully add a user. Please this user profiles');
                     redirect('admin/member/edit_member/'.$this->db->insert_id());
                }else{
                    $this->session->set_userdata('error_msg', 'Something wrong please try again');
                }  
             }else{
                 $data['validation_errors']=validation_errors('<p>', '</p>');
                  $this->session->set_userdata('error_msg', 'Something wrong please try again. Validation Error');
             }
            
        }
        $this->load->view('admin/member/add_new');
    }
    
    // public function 
    public function edit_member($user_id){
        $data['user_id']=$user_id;
        if($this->input->post('save_member')){
            $update_attr=array(
                'first_name'=>$this->input->post('first_name'),
                'last_name'=>$this->input->post('last_name'),
                'phone'=>$this->input->post('phone'),
                'email'=>$this->input->post('email'),
                'birthday'=>$this->input->post('birthday'),
                'address1'=>$this->input->post('address1'),
                'address2'=>$this->input->post('address2'),
                'country'=>$this->input->post('country'),
                'city'=>$this->input->post('city'),
                'state'=>$this->input->post('state'),
                'nid_number'=>$this->input->post('nid_number'),
                'passport_number'=>$this->input->post('passport_number')
            );
            
            if($this->db->where('id', $user_id)->update('users', $update_attr)){
                $this->session->set_userdata('success_msg', 'Your user hasbeen updated');
            }else{
                $this->session->set_userdata('error_msg', 'Something wrong please try again');
            }
        }
        
         $data['user_data']=$this->user_model->get_user($user_id);
        $this->load->view('admin/member/edit_member', $data);
    }
    
    
    // this fucntion for member share option 
    public function member_share($member_id=null){
        $data['user_data']=$this->user_model->get_all_user('member');
        if($this->input->get('member_search_id')){
            redirect('admin/member/member_share/'.$this->input->get('member_search_id'));
        }
        if($member_id){
            $data['user_data']=$this->user_model->get_all_user_by_id('member', $member_id);
        }
        
		$this->load->view("admin/member/member_share", $data);
    }
    
    
    
    
    /*This fucntion for edit user photo photo*/
    public function edit_photo($user_id){
        if($filename=$this->do_upload('profile_picture')){
            $update_attr=array( 'image'=>$filename );
            /*If file uploaded then insert file name in database*/
            if($this->db->where('id', $user_id)->update('users', $update_attr)){
                $this->session->set_userdata('success_msg', 'Your profile has been updated');
                 redirect('admin/member/edit_member/'.$user_id);
            }else{
                $this->session->set_userdata('error_msg', 'Something wrong please try again');
                 redirect('admin/member/edit_member/'.$user_id);
            }
        }else{
            $this->session->set_userdata('error_msg', 'File does note upload. Please try another image');
            redirect('admin/member/edit_member/'.$user_id);
        }
    }
    
     /*This funcito for delete room*/
    public function delete($id){
        if($this->db->where('id', $id)->delete('users')){
            $this->session->set_userdata('success_msg', 'You have successfully delete a user');
                redirect('admin/member/all_member');
        }else{
            $this->session->set_userdata('error_msg', 'Something went wrong please try again letter');
                redirect('admin/member/all_member');
        }
    }
    
    
    /*This function for change password*/
    public function change_password($user_id){
        if($this->input->post('change_password')){
            $update_attr=array( 'password'=>md5($this->input->post('password')) );
            if($this->db->where('id', $user_id)->update('users', $update_attr)){
                $this->session->set_userdata('success_msg', 'Your Password changed.');
                 redirect('admin/member/edit_member/'.$user_id);
            }else{
                $this->session->set_userdata('error_msg', 'Something wrong please try again');
                redirect('admin/member/edit_member/'.$user_id);
            }
        }
        $data['user_data']=$this->user_model->get_user($user_id);
        $data['user_id']=$user_id;
         $this->load->view('admin/member/change_password', $data);
    }
    
    
    //member details
    public function details($member_id=null, $share_id=null){
        if(!$member_id){redirect('admin/member/member_share');}
        
       
        
        $data['member_id']=$member_id;
         $data['user_data']=$this->user_model->get_all_user_by_id('member', $member_id);
         $data['share_list'] = $this->member_model->member_share_list($member_id);
        $data['savings_list'] = $this->member_model->member_savings_list($member_id);
        $data['loan_list'] = $this->member_model->member_loan_list($member_id);
        $this->load->view('admin/member/details', $data);
    }
    
    
    
    
    
    // member share
    public function share($member_id=null, $share_id=null){
        if(!$member_id){redirect('admin/member/member_share');}
        
        // share add
        if($this->input->post('save_new_share')){
            $attr=array(
                'member_id'=>$member_id,
                'share_number'=>$this->input->post('share_number'),
                'share_amount'=>$this->input->post('share_amount')
            );
            if($this->db->insert('member_share', $attr)){
                $this->session->set_userdata('success_msg', 'Success');
            }else{
                $this->session->set_userdata('error_msg', 'Something wrong pleasae try again');
            }
        }
        // share add
        if($this->input->post('save_share')){
            $attr=array(
                'share_number'=>$this->input->post('share_number'),
                'share_amount'=>$this->input->post('share_amount')
            );
            if($this->db->where('id', $share_id)->update('member_share', $attr)){
                $this->session->set_userdata('success_msg', 'Success');
            }else{
                $this->session->set_userdata('error_msg', 'Something wrong pleasae try again');
            }
            redirect('admin/member/share/'.$member_id);
        }
        
        if($share_id){
            $data['share_id']=$share_id;
            $data['share']=$this->member_model->share_by_id($share_id);
        }
        
        
        
        
        
        $data['member_id']=$member_id;
         $data['user_data']=$this->user_model->get_all_user_by_id('member', $member_id);
         $data['share_list'] = $this->member_model->member_share_list($member_id);
        $this->load->view('admin/member/share', $data);
    }
    
    
    //delete member share
    public function delete_member_share($id, $member_id){
       if( $this->db->where('id', $id)->delete('member_share')){
           $this->session->set_userdata('success_msg', 'Delete success');
       }else{
            $this->session->set_userdata('error_msg', 'Something wrong pleasae try again');
       }
        redirect('admin/member/share/'.$member_id);
    }
    
    // member saving 
    public function saving($member_id=null, $savings_id=null){
        if(!$member_id){redirect('admin/member/member_share');}
        
        // share add
        if($this->input->post('save_new_saving')){
            $attr=array(
                'member_id'=>$member_id,
                'saving_amount'=>$this->input->post('saving_amount'),
                'month'=>$this->input->post('saving_month'),
                'year'=>$this->input->post('saving_year'),
                'ref_number'=>$this->input->post('ref_number')
            );
            if($this->db->insert('savings', $attr)){
                $this->session->set_userdata('success_msg', 'Success');
            }else{
                $this->session->set_userdata('error_msg', 'Something wrong pleasae try again');
            }
        }
        // share add
        if($this->input->post('save_saving')){
            $attr=array(
               'saving_amount'=>$this->input->post('saving_amount'),
                'month'=>$this->input->post('saving_month'),
                'year'=>$this->input->post('saving_year'),
                'ref_number'=>$this->input->post('ref_number')
            );
            if($this->db->where('id', $savings_id)->update('savings', $attr)){
                $this->session->set_userdata('success_msg', 'Success');
            }else{
                $this->session->set_userdata('error_msg', 'Something wrong pleasae try again');
            }
            redirect('admin/member/saving/'.$member_id.'/'.$savings_id);
        }
        
        if($savings_id){
            $data['savings_id']=$savings_id;
            $data['saving']=$this->member_model->savings_by_id($savings_id);
        }
        
        
        
        
        $data['member_id']=$member_id;
         $data['user_data']=$this->user_model->get_all_user_by_id('member', $member_id);
         $data['savings_list'] = $this->member_model->member_savings_list($member_id);
        $this->load->view('admin/member/saving', $data);
    }
    
    //delete member saving
    public function delete_member_saving($id, $member_id){
       if( $this->db->where('id', $id)->delete('savings')){
           $this->session->set_userdata('success_msg', 'Delete success');
       }else{
            $this->session->set_userdata('error_msg', 'Something wrong pleasae try again');
       }
        redirect('admin/member/saving/'.$member_id);
    }
    
    
    // this method will load loan
    // member saving 
    public function loan($member_id=null, $loan_id=null){
        if(!$member_id){redirect('admin/member/member_share');}
        $total_save = $this->member_model->member_savings($member_id);
        $can_loan = ($total_save*300)/100;
        // share add
        if($this->input->post('save_new_loan')){
            
            if($this->input->post('amount') <= $can_loan){
                $attr=array(
                    'user_id'=>$member_id,
                    'amount'=>$this->input->post('amount')
                );
                if($this->db->insert('loan', $attr)){
                    $this->session->set_userdata('success_msg', 'Success');
                }else{
                    $this->session->set_userdata('error_msg', 'Something wrong pleasae try again');
                } 
            }else{
                $this->session->set_userdata('error_msg', 'Loan is grater then 300%');
            }
            
        }
        // share add
        if($this->input->post('save_loan')){
            if($this->input->post('amount') <= $can_loan){
                $attr=array(
                   'amount'=>$this->input->post('amount'),
                    'return_amount'=>$this->input->post('return_amount'),
                    'profit'=>$this->input->post('profit')
                );
                if($this->db->where('id', $loan_id)->update('loan', $attr)){
                    $this->session->set_userdata('success_msg', 'Success');
                }else{
                    $this->session->set_userdata('error_msg', 'Something wrong pleasae try again');
                }
            }else{
                $this->session->set_userdata('error_msg', 'Loan is grater then 300%');
            }
            
            
            redirect('admin/member/loan/'.$member_id);
        }
        
        if($loan_id){
            $data['loan_id']=$loan_id;
            $data['loan']=$this->member_model->loan_by_id($loan_id);
        }
        
        
        
        
        $data['member_id']=$member_id;
         $data['user_data']=$this->user_model->get_all_user_by_id('member', $member_id);
         $data['loan_list'] = $this->member_model->member_loan_list($member_id);
        $this->load->view('admin/member/loan', $data);
    }
    
    
    //delete member loan
    public function delete_member_loan($id, $member_id){
       if( $this->db->where('id', $id)->delete('loan')){
           $this->session->set_userdata('success_msg', 'Delete success');
       }else{
            $this->session->set_userdata('error_msg', 'Something wrong pleasae try again');
       }
        redirect('admin/member/loan/'.$member_id);
    }
    
    
    
    
    
    
    /*File upload function*/
 public function do_upload(){
        $path = './uploads/users/'; 
        $fileTmpName=$_FILES['user_image']["tmp_name"];
        $upload_dir = './uploads/users/'; 
        $file_name = $_FILES['user_image']['name'];
        $type = explode('.', $file_name);
        $type = $type[count($type)-1];
        $time = time();
        if( in_array($type, array('jpg', 'png', 'jpeg', 'gif', 'JPEG', 'PNG', 'JPEG', 'GIF' )) ){
            if( is_uploaded_file( $_FILES['user_image']['tmp_name'] ) ){
                move_uploaded_file( $_FILES['user_image']['tmp_name'], $upload_dir.$time.$file_name );
                return $time.$file_name;
            }
        }else{
            $this->session->set_userdata('error_msg', 'File type not supported');
            return false;
        }
   }
    
}// end of class

