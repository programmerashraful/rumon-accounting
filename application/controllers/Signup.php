<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->model('society');
    }
    //index method
    public function index(){
        redirect('signup/type');
    }
    public function type(){
        $this->load->view('frontend/sign-up-type');
    }
    
    // this function for sign up a member
	public function member()
	{
        $data = array();
        $erors=array();
        //$data['all_socity'] = $this->society->get_society();
        if($this->input->post('save_member')){
             $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
             $this->form_validation->set_rules('user_name', 'User Name', 'trim|required|is_unique[users.user_name]');
             $filename=$this->do_upload();
            if($filename){
                 $update_attr=array(
                    'first_name'=>$this->input->post('first_name'),
                    'last_name'=>$this->input->post('last_name'),
                    'user_name'=>$this->input->post('user_name'),
                    'birthday'=>$this->input->post('birthday'),
                    'address1'=>$this->input->post('address1'),
                    'city'=>$this->input->post('city'),
                    'state'=>$this->input->post('state'),
                    'country'=>$this->input->post('country'),
                    'password'=>md5($this->input->post('pass')),
                    'image'=>$filename,
                    'email'=>$this->input->post('email'),
                    'phone'=>$this->input->post('phone'),
                    'nid_number'=>$this->input->post('nid_number'),
                    'admission_fee'=>$this->user_model->get_setting_data('member_admission_fee'),
                    'type'=>'member'
                );

                 if($this->form_validation->run()){
                       if($this->db->insert('users', $update_attr)){
                        $this->session->set_userdata('success_msg', 'You have successfully add a user. Please this user profiles');
                         redirect('signup/success/');
                    }else{
                        $this->session->set_userdata('error_msg', 'Something wrong please try again');
                    }  
                 }else{
                     $data['validation_errors']=validation_errors('<p>', '</p>');
                      $this->session->set_userdata('error_msg', 'Something wrong please try again. Validation Error');
                 }
            }else{
                $this->session->set_userdata('error_msg', 'File is not possible to upload, pleas try another file');
            }
           
            
        }
        
		$this->load->view('frontend/sign-up-member', $data);
	}
    
    // this function for sign up a society
	public function society()
	{
        $data = array();
        $erors=array();
        //$data['all_socity'] = $this->society->get_society();
        if($this->input->post('save_society')){
             $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
             $this->form_validation->set_rules('user_name', 'User Name', 'trim|required|is_unique[users.user_name]');
             $filename=$this->do_upload();
            if($filename){
                
                 $update_attr=array(
                    'first_name'=>$this->input->post('first_name'),
                    'last_name'=>$this->input->post('last_name'),
                    'user_name'=>$this->input->post('user_name'),
                    'birthday'=>$this->input->post('birthday'),
                    'address1'=>$this->input->post('address1'),
                    'city'=>$this->input->post('city'),
                    'state'=>$this->input->post('state'),
                    'country'=>$this->input->post('country'),
                    'password'=>md5($this->input->post('pass')),
                    'image'=>$filename,
                    'email'=>$this->input->post('email'),
                    'phone'=>$this->input->post('phone'),
                    'nid_number'=>$this->input->post('nid_number'),
                    'admission_fee'=>$this->user_model->get_setting_data('member_admission_fee'),
                    'type'=>'society_admin'
                );

                 if($this->form_validation->run()){
                       if($this->db->insert('users', $update_attr)){
                           $insert_id = $this->db->insert_id();
                           if($this->add_society($insert_id)){
                               $this->session->set_userdata('success_msg', 'You have successfully add a user. Please this user profiles');
                               redirect('signup/success/');
                           }else{
                              $this->session->set_userdata('error_msg', 'Something wrong please try again');
                               
                               $this->db->where('id', $insert_id)->delete('users');
                           }
                        
                    }else{
                        $this->session->set_userdata('error_msg', 'Something wrong please try again');
                    }  
                 }else{
                     $data['validation_errors']=validation_errors('<p>', '</p>');
                      $this->session->set_userdata('error_msg', 'Something wrong please try again. Validation Error');
                 }
            }else{
                $this->session->set_userdata('error_msg', 'File is not possible to upload, pleas try another file');
            }
           
            
        }
        
		$this->load->view('frontend/sign-up-society', $data);
	}
    
    
    
    //Sign up success message
    public function success(){
        $this->load->view('admin/signup-success');
    }
    
    
    public function add_society($admin_id){
        $attr=array(
            'type'=>$this->input->post('type'),
            'admin_id'=>$admin_id,
            'name'=>$this->input->post('society_name'),
            'total_fund'=>$this->input->post('total_fund'),
            'share_rate'=>$this->input->post('share_rate'),
            'description'=>$this->input->post('society_description')
        );
        if($this->db->insert('society', $attr)){
            return true;
        }else{
            return false;
        }
    }
    
    
    /*This funcito for delete room*/
    public function delete($id){
        if($this->db->where('id', $id)->delete('users')){
            $this->session->set_userdata('success_msg', 'You have successfully delete a user');
                redirect('admin/users/all_user');
        }else{
            $this->session->set_userdata('error_msg', 'Something went wrong please try again letter');
                redirect('admin/users/all_user');
        }
    }
    
    
    
    /*File upload function*/
 public function do_upload(){
        $path = './uploads/users/'; 
        $fileTmpName=$_FILES['user_image']["tmp_name"];
        $upload_dir = './uploads/users/'; 
        $file_name = $_FILES['user_image']['name'];
        $type = explode('.', $file_name);
        $type = $type[count($type)-1];
        $time = time();
        if( in_array($type, array('jpg', 'png', 'jpeg', 'gif', 'JPEG', 'PNG', 'JPEG', 'GIF' )) ){
            if( is_uploaded_file( $_FILES['user_image']['tmp_name'] ) ){
                move_uploaded_file( $_FILES['user_image']['tmp_name'], $upload_dir.$time.$file_name );
                return $time.$file_name;
            }
        }else{
            $this->session->set_userdata('error_msg', 'File type not supported');
            return false;
        }
   }
    
  
}
