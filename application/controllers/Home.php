<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->model('society');
    }
    
    //index method
	public function index()
	{
        $data = array();
        $data['all_socity'] = $this->society->get_society('public');
		$this->load->view('frontend/home', $data);
	}
    
    
  
}
