<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Member_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('member_model');
        
    }
	public function index()
	{ 
        $member_id=$this->session->userdata('current_user_id');
        $data['member_id']=$member_id;
        if($this->input->post('user_name')){
            $member = $this->user_model->get_user_by_user_name($this->input->post('user_name'));
            if($member){
                $member_id = $member->id;
                $data['member_id'] = $member->id;
                $data['user_data']=$this->user_model->get_all_user_by_id('member', $member_id);
                $data['share_list'] = $this->member_model->member_share_list($member_id);
                $data['savings_list'] = $this->member_model->member_savings_list($member_id);
                $data['loan_list'] = $this->member_model->member_loan_list($member_id);
            }else{
                $data['user_data']=null;
                $data['share_list'] = null;
                $data['savings_list'] = null;
                $data['loan_list'] = null;
            }
        }else{
            $data['user_data']=$this->user_model->get_all_user_by_id('member', $member_id);
            $data['share_list'] = $this->member_model->member_share_list($member_id);
            $data['savings_list'] = $this->member_model->member_savings_list($member_id);
            $data['loan_list'] = $this->member_model->member_loan_list($member_id);
        }
        
        $this->load->view('admin/member/details', $data);
	}
    
    

    
    
}

