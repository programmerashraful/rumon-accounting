<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {
    public function __construct(){
        parent::__construct();
    }
	public function index($error=null)
	{
        $this->destroy();
	}
    
    public function destroy(){
        $sesattr = array(
						'current_user_id' => '',
						'current_username' => '',
						'current_user_type' => '',
						'base_url' => ''
					);
       $this->session->set_userdata($sesattr);
        redirect('login');
    }
}
