<?php
class User_Model extends CI_Model{
    public function __construct(){
        parent:: __construct();
        $this->user_id=$this->session->userdata('current_user_id');
        $this->user_type=$this->session->userdata('current_user_type');
    }
    public function is_user_logd_in(){
        if($this->session->userdata('current_user_id') and base_url()==$this->session->userdata('base_url'))
        {return true;}
        else{return false;}
    }
    public function is_captcha_log(){
        return true;
    }
    
    
    // is user log in and make log in session
    public function is_captcha_available( $username, $captcha_code ){
        $attr = array(
            'user_name' => $username
        );
        $result = $this->db->get_where('users', $attr);
        //if username amd password is metch it redirect dashboard else redirect to login page
			if($captcha_code==config_item('captcha_code')){
                if($result->num_rows() > 0){
                    $sesattr = array(
                            'captcha_code' => true,
                            'base_url' => base_url(),
                        );
                    //session set 
                    $this->session->set_userdata($sesattr);
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
    }
    
    // is user log in and make log in session
    public function is_user_available( $username, $password ){
        $attr = array(
            'user_name' => $username,
			'password'  => $password
        );
        $result = $this->db->get_where('users', $attr);
        //if username amd password is metch it redirect dashboard else redirect to login page
			if($result->num_rows() > 0){
				$sesattr = array(
						'current_user_id' => $result->row(0)->id,
						'current_username' => $username,
						'current_user_type' => $result->row(0)->type,
						'base_url' => base_url()
					);
				//session set 
				$this->session->set_userdata($sesattr);
                return true;
			}else{
				return false;
			}
    }
    
/*
========================================================================================
                    USER LOG IN SECTION EDD HERE
========================================================================================
*/
   
     public function get_all_user($type=null, $per_page=null, $page=null){
         
         if($this->user_type=='member'){
            $this->db->where('id', $this->user_id);
        }
         
        $result=0;
        if($per_page!=null){
            $this->db->order_by("id", "DESC");
            $this->db->limit($per_page, $page);
        }
        if ($type==null){
            $result = $this->db->get('users');
        }else{
            $result = $this->db->get_where('users', array('type'=>$type));
        }
        return $result->result();
        
    }
   
     public function get_all_user_by_id($type=null, $member_id=null){
         
         $result = $this->db->get_where('users', array('type'=>$type, 'id'=>$member_id));
        return $result->result();
        
    }
    
    
    
    
    /*This function for get all user count */
    public function get_user_count($type=null){
        $result=0;
        
        if ($type==null){
            $result = $this->db->get('users');
        }else{
            $result = $this->db->get_where('users', array('type'=>$type));
        }
        return $result->num_rows();
        
    }
    
    //thhis function for query all members
     public function get_all_new_agent($per_page=null, $page=null){
        $result=0;
        if($per_page!=null){
            $this->db->order_by("id", "ASC");
            $this->db->limit($per_page, $page);
        }
        $result = $this->db->get_where('users', array('type'=>'agent'));
        return $result->result();
        
    }
    
    /*This function for get all members count */
    public function get_agent_count($type=null){
        $result=0;
        
        $result = $this->db->get_where('users', array('type'=>'agent'));
        return $result->num_rows();
        
    }
    
    /*This fnction for get one single user*/
    public function get_user($id=null){
         $result = $this->db->get_where('users', array('id'=>$id));
        return $result->row(0);
    }
    /*This fnction for get one single user*/
    public function get_user_by_user_name($user_name=null){
         $result = $this->db->get_where('users', array('user_name'=>$user_name));
        return $result->row(0);
    }
    

    
    /*This fucntion for get single setting data*/
    public function get_setting_data($data_id){
        $result = $this->db->get_where('setting', array('data_id'=>$data_id));
        if($result->row(0)){
            $data = $result->row(0);
            return  $data->data;
        }else{
            return '';
        }
    }
    
    
    
    public function member_name($id=null){
        $user = $this->get_user($id);
        $user_name = 'No Agent';
        if($user){
            $user_name=$user->first_name.' '.$user->last_name;
        }
        return $user_name;
    } 
    

    
}
?>