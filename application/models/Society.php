<?php
class Society extends CI_Model{
    public function __construct(){
        parent:: __construct();
      
    }
   
    // return all society 
     public function get_society($type=null, $per_page=null, $page=null){
         
       
        $result=0;
        if($per_page!=null){
            $this->db->order_by("id", "DESC");
            $this->db->limit($per_page, $page);
        }
        if($type){
            $this->db->where('type', $type);
        }
        $result = $this->db->get('society');
        return $result->result();
        
    }
    
    // count homany members of a society
    public function count_member($society_id){
        $result = $this->db->get_where('society_members', array('society_id'=>$society_id));
        return $result->num_rows();
    }
   
     
    
}
?>