<?php
class Cost_model extends CI_Model{
    public function __construct(){
        parent:: __construct();
        $this->user_id=$this->session->userdata('current_user_id');
        $this->user_type=$this->session->userdata('current_user_type');
    }
   
    
    // member all share
    public function cost_list(){
        $this->db->order_by('id', 'DESC');
        $result = $this->db->get('cost');
        return $result->result();
    }
    
    
    // member share by share id
    public function cost_by_id($cost_id){
        $result = $this->db->get_where('cost', array('id'=>$cost_id));
        return $result->row(0);
    }
    
    //  total cost
    public function total_cost(){
        
        $result=null;
        $this->db->select_sum('amount');
        $result = $this->db->get('cost');
        $result = $result->result();
        if($result[0]->amount){
            return $result[0]->amount;
        }else{
          return 0;  
        }
    }
   
    
}
?>