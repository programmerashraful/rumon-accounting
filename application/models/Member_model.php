<?php
class Member_Model extends CI_Model{
    public function __construct(){
        parent:: __construct();
        $this->user_id=$this->session->userdata('current_user_id');
        $this->user_type=$this->session->userdata('current_user_type');
    }
   
    //member add share amount
    public function admission_fees($id=null){
        $total_amount=null;
        $result=null;
        if($id){
            $this->db->select_sum('admission_fee');
            $result = $this->db->get_where('users', array('id'=>$id, 'type'=>'member'));
        }else{
            $this->db->select_sum('admission_fee');
            $result = $this->db->get_where('users', array('type'=>'member'));
        }
        $result = $result->result();
         if($result[0]->admission_fee){
            return $result[0]->admission_fee;
        }else{
          return 0;  
        }
        
    }
    //member add share amount
    public function member_share_amount($id=null){
        $total_amount=null;
        $result=null;
        if($id){
            $this->db->select_sum('share_amount');
            $result = $this->db->get_where('member_share', array('member_id'=>$id));
        }else{
            $this->db->select_sum('share_amount');
            $result = $this->db->get('member_share');
        }
        $result = $result->result();
         if($result[0]->share_amount){
            return $result[0]->share_amount;
        }else{
          return 0;  
        }
        
    }
   
    //member add share number
    public function member_share_number($id=null){
        $total_amount=null;
        $result=null;
        if($id){
            $this->db->select_sum('share_number');
            $result = $this->db->get_where('member_share', array('member_id'=>$id));
        }else{
            $this->db->select_sum('share_number');
            $result = $this->db->get('member_share');
        }
        $result = $result->result();
        if($result[0]->share_number){
            return $result[0]->share_number;
        }else{
          return 0;  
        }
        
    }
    
    //This method for count members savings 
    public function member_savings($id=null){
        $total_amount=null;
        $result=null;
        if($id){
            $this->db->select_sum('saving_amount');
            $result = $this->db->get_where('savings', array('member_id'=>$id));
        }else{
            $this->db->select_sum('saving_amount');
            $result = $this->db->get('savings');
        }
        $result = $result->result();
        
        if($result[0]->saving_amount){
            return $result[0]->saving_amount;
        }else{
          return 0;  
        }
    }
    
    //This method for count member loan
    public function member_loans($id=null){
        $total_amount=null;
        $result=null;
        if($id){
            $this->db->select_sum('amount');
            $result = $this->db->get_where('loan', array('user_id'=>$id, 'return_amount<='=>0));
        }else{
            $this->db->select_sum('amount');
            $result = $this->db->get_where('loan', array('return_amount<='=>0));
        }
        $result = $result->result();
        
        if($result[0]->amount){
            return $result[0]->amount;
        }else{
          return 0;  
        }
    }
    
    //This method for count member loan profit
    public function member_loan_profit($id=null){
        $total_amount=null;
        $result=null;
        if($id){
            $this->db->select_sum('profit');
            $result = $this->db->get_where('loan', array('user_id'=>$id, 'return_amount>'=>0));
        }else{
            $this->db->select_sum('profit');
            $result = $this->db->get_where('loan', array('return_amount>'=>0));
        }
        $result = $result->result();
        
        if($result[0]->profit){
            return $result[0]->profit;
        }else{
          return 0;  
        }
    }
    
    //this fucntin for return loan list
    public function member_loan_list($member_id=null){
        if($member_id!=null)
            $result=$this->db->get_where('loan', array('user_id'=>$member_id));
        else
            $result=$this->db->get('loan');
        return $result->result();
        
    }
    
    // member all share
    public function member_share_list($member_id){
        $result = $this->db->get_where('member_share', array('member_id'=>$member_id));
        return $result->result();
    }
    // member all share
    public function member_savings_list($member_id){
        $result = $this->db->get_where('savings', array('member_id'=>$member_id));
        return $result->result();
    }
    
    // member share by share id
    public function share_by_id($share_id){
        $result = $this->db->get_where('member_share', array('id'=>$share_id));
        return $result->row(0);
    }
    // member savings by savings id
    public function savings_by_id($savings_id){
        $result = $this->db->get_where('savings', array('id'=>$savings_id));
        return $result->row(0);
    }
    // member loan by savings id
    public function loan_by_id($loan_id){
        $result = $this->db->get_where('loan', array('id'=>$loan_id));
        return $result->row(0);
    }
    
}
?>